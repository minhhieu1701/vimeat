/**
 * @typedef {Object} EnumItem
 * @property {*} value
 * @property {string} text
 */
const EnumItem;

module.exports = EnumItem;
