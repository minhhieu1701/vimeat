class PhotoItem{
    constructor() {
        this.height = 0;
        this.width = 0;
        this.url = "";
    }
}

module.exports = PhotoItem;

/**
 * @typedef {Object} PhotoItem
 * @property {number} height
 * @property {number} width
 * @property {string} height
 */