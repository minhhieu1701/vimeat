const actionResultCode = require("../../constant/action-result-code");
const Joi = require("joi");

/**
 * @typedef {Object} Code
 * @property {string} text
 * @property {number} value
 */

class JsonResult {
  constructor() {
    this.code = actionResultCode.INVALID_PARAM.value;
    this.message = actionResultCode.INVALID_PARAM.text;
    this.data = null;
  }

  /**
   *
   * @param {Code} resultCode
   * @param {Object} data
   */
  bind(resultCode, data) {
    this.code = resultCode.value;
    this.message = resultCode.text;
    this.data = data;
  }

  /**
   *
   * @param {Object} data
   */
  ok(data) {
    this.code = actionResultCode.SUCCESS.value;
    this.message = actionResultCode.SUCCESS.text;
    this.data = data;
  }

  /**
   *
   * @param {Object} data
   */
   fail(data) {
    this.code = actionResultCode.FAIL.value;
    this.message = actionResultCode.FAIL.text;
    this.data = data;
  }

  

  /**
   *
   * @param {Object} data
   */
   invalidParam(data) {
    this.code = actionResultCode.INVALID_PARAM.value;
    this.message = actionResultCode.INVALID_PARAM.text;
    this.data = data;
  }
}

module.exports = JsonResult;
module.exports.swaggerResponse = Joi.object().keys({
  code: Joi.number(),
  message: Joi.string(),
  data: Joi.any(),
});
