/**
 * @typedef {Object} GetList
 * @property {number} totalCount
 * @property {number} totalRemain
 * @property {Array<any>} records
 */

/**
 * @type {GetList}
 */
const GetList;

module.exports = GetList;