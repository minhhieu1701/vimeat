/**
 * @typedef {Object} Detail
 * @property {number} id
 * @property {string} name
 * @property {number} unitPrice
 * @property {boolean} havePromotion
 * @property {number | null} salePrice
 * @property {number} unit
 * @property {string} shortDescription
 * @property {string} description
 * @property {boolean} isActive
 * @property {string} publishFrom
 * @property {string} publishTo
 * @property {Array<>} publishTo
 * @property {DateTime} createdAt
 * @property {number} createdBy
 */

/**
 * @type {Detail}
 */
const Detail;

module.exports = Detail;

/** @typedef {import("../shared/photo-item").PhotoItem} PhotoItem*/
