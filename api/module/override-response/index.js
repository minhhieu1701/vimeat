const JsonResult = require("../../models/shared/json-result");
const actionResultCode = require("../../constant/action-result-code");

module.exports = {
  UnAuthorized: (server) => {
    server.ext("onPreResponse", (request, reply) => {
      if(request.response.contentType === "image/png"){
        return reply.continue;
      }
      if (request.response.isBoom) {
        const err = request.response;
        const errName = err.output.payload.error;
        const statusCode = err.output.payload.statusCode;

        console.log(err);

        const data = new JsonResult();

        switch (statusCode) {
          case 401:
            data.code = actionResultCode.UNAUTHORIZED.value;
            break;
          case 400:
            data.code = actionResultCode.BAD_REQUEST.value;
            break;
          case 403:
            data.code = actionResultCode.FORBIDDEN.value;
            break;
          case 404:
            data.code = actionResultCode.NOT_FOUND.value;
            break;
          default:
            data.code = actionResultCode.FAIL.value;
            break;
        }

        data.message = errName;

        const response = reply.response(data).code(200);
        response.type("application/json");
        return response;
      }

      return reply.continue;
    });
  },
};
