const swaggerOption = {
  auth: false,
  securityDefinitions: {
    jwt: {
      type: "apiKey",
      name: "Authorization",
      in: "header",
    },
  },
  security: [{ jwt: [] }],
  info: {
    title: "Vimeat API Documentation",
    version: "1.0",
    contact: {
      name: "Nguyen Duong Minh Hieu",
      email: "nguyenduongminhhieu1701@gmail.com",
    },
  },
  expanded: "none",
  documentationPath: process.env.SWAGGER_DEFAULT_PATH || "/documentation",
  //uiCompleteScript: `var x = document.querySelector("form.download-url-wrapper");x.querySelector('.download-url-input').remove();var select = document.createElement("select");select.className = "download-url-input";var option = document.createElement("option");option.text = "Admin";option.value = "admin";select.add(option);var option1 = document.createElement("option");option1.text = "Api";option1.value = "api";select.add(option1);x.prepend(select);`,
};

//https://github.com/glennjones/hapi-swagger/blob/master/optionsreference.md
module.exports = swaggerOption;
