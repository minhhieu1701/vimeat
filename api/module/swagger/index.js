const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('../../my-package/hapi-swagger');
const swaggerOption = require('./option');
const routes = require('../../routes');


module.exports = {
    init: async(server) => {
        if (!server) {
            throw new Error("Please setup your sever to continue. Thanks!");
        }

        server.route(routes);

        await server.register([
            Inert,
            Vision,
            {
                plugin: HapiSwagger,
                options: swaggerOption
            }
        ]);
    }
}