const Jwt = require("jsonwebtoken");
const hapiAuthJWT = require("hapi-auth-jwt2");
const $ = require("lodash");

/**
 * @typedef {import('../mysql/models/user').User} User
 */

const JWT = {
  /**
   * Create jwt token from user information
   * @param {User} user
   * @returns {string} jwt token
   */
  createToken: (user) => {
    const SCOPE = "admin";

    return Jwt.sign(
      { id: user.uid, username: user.username, scope: SCOPE },
      process.env.JWT_SECRET_KEY,
      {
        algorithm: "HS512",
        expiresIn: "1d",
      }
    );
  },
  init: async (server) => {
    // Register jwt with the server

    await server.register(hapiAuthJWT);

    server.auth.strategy("jwt", "jwt", {
      key: process.env.JWT_SECRET_KEY,
      validate: JWT.validate,
    });

    // Set the strategy
    server.auth.default("jwt");
  },
  validate: async function (decoded, request, h) {
    // console.log(" - - - - - - - decoded token:");
    // console.log(decoded);
    // console.log(" - - - - - - - request info:");
    // console.log(request.info);
    // console.log(" - - - - - - - user agent:");
    // console.log(request.headers["user-agent"]);

    const currentTimestamp = new Date().getTime();

    // do your checks to see if the person is valid
    if ($.isEmpty(decoded) === true || decoded.scope !== "admin") {
      return { isValid: false };
    } else if (+decoded.exp * 1000 < currentTimestamp) {
      return { isValid: false };
    } else {
      return { isValid: true };
    }
  },
};

module.exports = JWT;
