const { DataTypes } = require("sequelize");
const MySql = require("..");

const db = MySql.init();

const Setting = db.define(
  "setting",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: true,
    },
  },
  {
    timestamps: false,
    tableName: "setting",
  }
);

module.exports = Setting;

/**
 * @typedef {Object} Setting
 * @property {number} id
 * @property {string} key
 * @property {string} value
 */
