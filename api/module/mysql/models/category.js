const { DataTypes } = require("sequelize");
const MySql = require("..");

const db = MySql.init();

const Category = db.define(
  "category",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    background: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    border_color: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    tableName: "category",
  }
);

module.exports = Category;

/**
 * @typedef {Object} Category
 * @property {number} id
 * @property {string} title
 * @property {string} photo
 * @property {string} background
 * @property {string} border_color
 * @property {boolean} is_active
 */