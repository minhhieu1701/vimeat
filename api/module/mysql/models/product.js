const { DataTypes } = require("sequelize");
const MySql = require("..");

const db = MySql.init();

const Product = db.define(
  "product",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    unit_price: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    have_promotion: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    sale_price: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    unit: {
      type: DataTypes.TINYINT,
      allowNull: false,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    short_description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    weight: {
      type: DataTypes.DECIMAL,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    tableName: "product",
  }
);

module.exports = Product;

/**
 * @typedef {Object} Product
 * @property {boolean} have_promotion
 * @property {boolean} is_active
 * @property {Date | null} publish_from
 * @property {Date | null} publish_to
 * @property {Date} created_at
 * @property {number | null} sale_price
 * @property {number} created_by
 * @property {number} id
 * @property {number} unit
 * @property {number} unit_price
 * @property {number} weight
 * @property {string} description
 * @property {string} name
 * @property {string} short_description
 * @property {string} url_rewrite
 */
