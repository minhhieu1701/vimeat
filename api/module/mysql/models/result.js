
/**
 * @typedef {Object} Result
 * @property {number} code 
 * @property {string} description
 * @property {string} data
 */
const Result;

module.exports = Result;