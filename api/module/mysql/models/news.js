const { DataTypes } = require("sequelize");
const MySql = require("../../mysql");

const db = MySql.init();

const News = db.define(
  "news",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
    },
    url_rewrite: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    publish_from: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    publish_to: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    created_by: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    tableName: "news",
  }
);

module.exports = News;

/**
 * @typedef {Object} News
 * @property {number} id
 * @property {string} uid
 * @property {string} url_rewrite
 * @property {string} title
 * @property {string} description
 * @property {DateTime} publish_from
 * @property {DateTime} publish_to
 * @property {string} photo
 * @property {boolean} is_active
 * @property {DateTime} created_at
 * @property {number} created_by
 */