const { DataTypes } = require("sequelize");
const MySql = require("../../mysql");

const db = MySql.init();

const User = db.define(
  "user",
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    uid: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: false,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    salt: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    tableName: "user",
  }
);

module.exports = User;

/**
 * @typedef {Object} User
 * @property {number} id
 * @property {string} uid
 * @property {string} username
 * @property {string} password
 * @property {string} salt
 * @property {string} first_name
 * @property {string} last_name
 * @property {boolean} is_active
 */