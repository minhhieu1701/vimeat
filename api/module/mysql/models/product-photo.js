const { DataTypes } = require("sequelize");
const MySql = require("..");

const db = MySql.init();

const ProductPhoto = db.define(
  "product_photo",
  {
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    display_order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
  },
  {
    timestamps: false,
    tableName: "product_photo",
  }
);

module.exports = ProductPhoto;

/**
 * @typedef {Object} ProductPhoto
 * @property {number} product_id
 * @property {string} photo
 * @property {number} display_order
 */