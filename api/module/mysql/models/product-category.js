const { DataTypes } = require("sequelize");
const MySql = require("..");

const db = MySql.init();

const ProductCategory = db.define(
  "product_category",
  {
    product_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    }
  },
  {
    timestamps: false,
    tableName: "product_category",
  }
);

module.exports = ProductCategory;

/**
 * @typedef {Object} ProductCategory
 * @property {number} product_id
 * @property {number} category_id
 */