module.exports = {
  getDisplayName: (firstName, lastName) => {
    if (!firstName) {
      return lastName;
    } else if (!lastName) {
      return firstName;
    }
    return `${firstName} ${lastName}`;
  },
};
