const StringBuffer = require("../../util/string-buffer");
const $ = require("lodash");

module.exports = {
  /**
   * @param {string} storeProcedureName Name of the proc
   * @param {Array} params Array of value orderly passed to proc
   */
  executeByProc: async function (storeProcedureName, sequelize, params) {
    const str = new StringBuffer();

    str.append("CALL `").append(storeProcedureName).append("` (");

    if ($.isEmpty(params) === true) {
      str.append(")");
    } else {
      for (let index = 0, length = params.length; index < length; index++) {
        if($.isBoolean(params[index]) === true){
          str.append(params[index]);
        }
        else if(!params[index] && params[index]!== 0){
          str.append(' null');
        }
        else{
          str.append("'").append(params[index]).append("'");
        }

        if (index < length - 1) {
          str.append(", ");
        }
      }

      str.append(")");
    }

    const results = await sequelize.query(str.toString());

    if($.isEmpty(results) === true){
      return null;
    }

    return results;
  },
};
