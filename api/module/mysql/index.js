const { Sequelize } = require("sequelize");

const mysql = {
  init: () => {
    const sequelize = new Sequelize(
      process.env.MYSQL_DATABASE,
      process.env.MYSQL_USERNAME,
      process.env.MYSQL_PASSWORD,
      {
        host: process.env.MYSQL_HOST,
        dialect: "mysql",
      }
    );

    return sequelize;
  },
  start: async () => {
    const connection = mysql.init();

    try {
      await connection.authenticate();
      console.log("Connection has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  },
};

module.exports = mysql;
