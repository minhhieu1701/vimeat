const DEFAULT_NO_IMAGE_SIZE = {
  name: "no-image.jpg",
  url: `${process.env.MEDIA_HOST_URL}/image/no-image.jpg`,
  height: 0,
  width: 0,
};

module.exports.getImageUrl = (folderName, photo, sizes) => {
  if (!folderName || !photo) {
    return [DEFAULT_NO_IMAGE_SIZE];
  }

  if (!sizes || sizes.length < 1) {
    sizes = [
      {
        height: 600,
        width: 400,
      },
    ];
  }

  const arr = photo.split("/");

  if (arr.length < 2) {
    return [DEFAULT_NO_IMAGE_SIZE];
  }

  const result = [];

  sizes.forEach((size) => {
    let sSize = "";
    if (size[0] === 0 && size[1] === 0) {
      sSize = "s";
    } else if (size[0] === 0) {
      sSize = size[1];
    } else if (size[1] === 0) {
      sSize = size[0];
    } else {
      sSize = `${size[0]}x${size[1]}`;
    }

    result.push({
      name: photo,
      url: `${process.env.MEDIA_HOST_URL}/image/${folderName}/${arr[0]}/${sSize}/${arr[1]}`,
      height: size[1],
      width: size[0],
    });
  });

  return result;
};

module.exports.getNewsPhotoUrl = (photo, sizes) => {
  return this.getImageUrl("news", photo, sizes);
};

module.exports.getCategoryPhotoUrl = (photo, sizes) => {
  return this.getImageUrl("category", photo, sizes);
};

module.exports.getProductPhotoUrl = (photo, sizes) => {
  return this.getImageUrl("product", photo, sizes);
};
