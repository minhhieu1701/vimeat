module.exports.GetPageSize = function (limitItem, totalItem) {
  return Math.ceil(totalItem / limitItem);
};

/**
 * @param {number} itemPerPage
 * @param {number} page
 * @returns {{
 *     startIndex: number
 *     endIndex: number
 * }}
 */
module.exports.Partition = function (itemPerPage, page, totalItem) {
  const result = {
    startIndex: (page - 1) * itemPerPage,
    endIndex: page * itemPerPage,
  };

  if (result.endIndex > totalItem) {
    result.endIndex = totalItem;
  }

  return result;
};
