module.exports = {
  /**
   * Check function is the async function or not
   * @param {Function} func
   * @returns {boolean} true | false
   */
  isAsyncFunction: (func) => {
    if (!func) {
      return false;
    }

    if (func.constructor.name == "AsyncFunction") {
      return true;
    }

    return false;
  },
};
