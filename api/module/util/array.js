module.exports = {
  /**
   * @param {Array<number>} array
   * @param {number} targetValue
   */
  getClosestValue: (array, targetValue) => {
    return array.sort(
      (a, b) => Math.abs(targetValue - a) - Math.abs(targetValue - b)
    )[0];
  },
};
