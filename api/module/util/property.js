module.exports = {
  /**
   * @param {Array<string>} names array of property name
   * @param {Object} object target object want to remove property
   */
  removeProperty: (names, object) => {
    if (!names || names.length < 1) {
      return false;
    }

    names.forEach((name) => {
      delete object[name];
    });

    return true;
  },
  /**
   * @param {Array<any>} objectBoolean
   * @returns {boolean}
   */
  bindBoolean: (objectBoolean) => {
    if (objectBoolean && objectBoolean.length > 0 && objectBoolean[0] === 1) {
      return true;
    }

    return false;
  },
};
