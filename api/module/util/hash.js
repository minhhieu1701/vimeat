const HashType = require("../../constant/hash-type");
const md5 = require("md5");

module.exports.HASH_TYPE = HashType;

module.exports.Hash = {
    /**
     * Hash password
     * @param {Number} type Hash type
     * @param {string} password Your password
     * @param {string} salt The salt you want to make the secret key hash
     * @returns {string} Token
     */
    password: (type, password, salt)=>{
        if(!type){
            throw new Error("Hash type can not be null");
        }
        else if(!password || !salt){
            throw new Error("Input is invalid, make sure two param: password and salt have value");
        }

        let str = "";
        switch (type) {
            case HashType.MD5:
                return md5(`${password}${salt}`);
            default:
                return str;
        }
    }
    ,
    /**
     * Hash string to Base64
     * @param {string} str Your string
     * @returns {string} encoded base64
     */
    stringToBase64: (str) => {
        return Buffer.from(str).toString('base64');
    }
}