module.exports = {
  SUCCESS: {
    text: "Success",
    value: 0,
  },
  INVALID_PARAM: {
    text: "Invalid param, please try again!",
    value: -2,
  },
  INVALID_CHECKSUM: {
    text: "Request is invalid, please try again!",
    value: -6,
  },
  FAIL: {
    text: "Action failed, please try again",
    value: -1,
  },
  INVALID_EMAIL: {
    text: "Invalid email, please try again",
    value: -5,
  },
  FORBIDDEN: {
    text: "Invalid login, please check your information again",
    value: 403,
  },
  UNAUTHORIZED: {
    text: "Unauthorized",
    value: 401,
  },
  BAD_REQUEST: {
    text: "Bad request",
    value: 400,
  },
  NOT_FOUND: {
    text: "Your source which you requested was not found, please check again",
    value: 404,
  },
};

/**
 * @typedef {Object} ResultCode
 * @property {string} text
 * @property {number} value
 */
