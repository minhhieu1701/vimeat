const SettingModel = require("../module/mysql/models/setting");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");

class SettingRepository {
  constructor() {
    this._rpSetting = SettingModel;
  }

  /**
   *
   * @param {number} id
   * @param {boolean | null} isActive
   * @return {Promise<Category>} news
   */
  async get(id) {
    const result = await DbExtension.executeByProc(
      "Setting_Get",
      SettingModel.sequelize,
      [id]
    );

    if (result && result.length > 0) {
      return result[0];
    }

    return null;
  }

  async getIdListByKey(key) {
    if ($.isEmpty(key) === true) {
      return null;
    }

    return await DbExtension.executeByProc(
      "Setting_GetIdListByKey",
      SettingModel.sequelize,
      [key]
    );
  }

  /**
   *
   * @param {boolean | null} isActive
   * @return {Promise<Array<string>>} news
   */
  async getIdList() {
    return await DbExtension.executeByProc(
      "Setting_GetIdList",
      SettingModel.sequelize,
      []
    );
  }

  /**
   *
   * @param {string} ids format: "abc,cde,efg"
   * @param {boolean | null} isActive
   * @return {Promise<Array<Category>>} news
   */
  async getListByIds(ids) {
    return await DbExtension.executeByProc(
      "Setting_GetListByIds",
      SettingModel.sequelize,
      [ids]
    );
  }

  /**
   *
   * @param {Category} entity
   * @return {Promise<boolean>} result true or false
   */
  async insert(entity) {
    const result = await DbExtension.executeByProc(
      "Setting_Insert",
      SettingModel.sequelize,
      [
        entity.key,
        entity.value,
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {Category} entity
   * @return {Promise<boolean>} result true or false
   */
  async update(entity) {
    const result = await DbExtension.executeByProc(
      "Setting_Update",
      SettingModel.sequelize,
      [
        entity.id,
        entity.value
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {number} id
   * @return {Promise<boolean>} result true or false
   */
  async delete(id) {
    const result = await DbExtension.executeByProc(
      "Setting_Delete",
      SettingModel.sequelize,
      [id]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = SettingRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/setting').Setting} Setting
 */
