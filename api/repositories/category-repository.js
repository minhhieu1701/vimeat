const CategoryModel = require("../module/mysql/models/category");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");

class CategoryRepository {
  constructor() {
    this._rpCategory = CategoryModel;
  }

  /**
   *
   * @param {number} id
   * @param {boolean | null} isActive
   * @return {Promise<Category>} news
   */
  async get(id, isActive = null) {
    const result = await DbExtension.executeByProc(
      "Category_Get",
      CategoryModel.sequelize,
      [id, isActive]
    );

    if (result && result.length > 0) {
      return result[0];
    }

    return null;
  }

  /**
   *
   * @param {boolean | null} isActive
   * @return {Promise<Array<Category>>} news
   */
  async getList(isActive = true) {
    return await DbExtension.executeByProc(
      "Category_GetList",
      CategoryModel.sequelize,
      [isActive]
    );
  }

  /**
   *
   * @param {boolean | null} isActive
   * @return {Promise<Array<string>>} news
   */
  async getIdList(isActive = true) {
    return await DbExtension.executeByProc(
      "Category_GetIdList",
      CategoryModel.sequelize,
      [isActive]
    );
  }

  /**
   *
   * @param {string} ids format: "abc,cde,efg"
   * @param {boolean | null} isActive
   * @return {Promise<Array<Category>>} news
   */
  async getListByIds(ids, isActive = true) {
    return await DbExtension.executeByProc(
      "Category_GetListByIds",
      CategoryModel.sequelize,
      [ids, isActive]
    );
  }

  /**
   *
   * @param {Category} entity
   * @return {Promise<boolean>} result true or false
   */
  async insert(entity) {
    const result = await DbExtension.executeByProc(
      "Category_Insert",
      CategoryModel.sequelize,
      [
        entity.name,
        entity.photo,
        entity.is_active,
        entity.background,
        entity.border_color,
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {Category} entity
   * @return {Promise<boolean>} result true or false
   */
  async update(entity) {
    const result = await DbExtension.executeByProc(
      "Category_Update",
      CategoryModel.sequelize,
      [
        entity.id,
        entity.name,
        entity.is_active,
        entity.photo,
        entity.background,
        entity.border_color,
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {number} id
   * @return {Promise<boolean>} result true or false
   */
  async delete(id) {
    const result = await DbExtension.executeByProc(
      "Category_Delete",
      CategoryModel.sequelize,
      [id]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = CategoryRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/category').Category} Category
 */
