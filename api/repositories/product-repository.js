const ProductModel = require("../module/mysql/models/product");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");
const sequelize = require("sequelize");

class ProductRepository {
  constructor() {
    this._rpNews = ProductModel;
  }

  /**
   *
   * @param {number} id
   * @param {boolean | null} isActive
   * @return {Promise<Product>} product
   */
  async get(id, isActive = null) {
    const result = await DbExtension.executeByProc(
      "Product_Get",
      ProductModel.sequelize,
      [id, isActive]
    );

    if (result && result.length > 0) {
      return result[0];
    }

    return null;
  }

  /**
   *
   * @param {boolean | null} isActive
   * @return {Promise<Array<string>>} product
   */
  async getIdList(isActive = true) {
    return await DbExtension.executeByProc(
      "Product_GetIdList",
      ProductModel.sequelize,
      [isActive]
    );
  }

  /**
   *
   * @param {string} ids format: "abc,cde,efg"
   * @param {boolean | null} isActive
   * @return {Promise<Array<Product>>} product
   */
  async getListByIds(ids) {
    return await DbExtension.executeByProc(
      "Product_GetListByIds",
      ProductModel.sequelize,
      [ids]
    );
  }

  async getIdListByKeyword(keyword, isActive = true) {
    if ($.isEmpty(keyword) === true) {
      return null;
    }

    return await DbExtension.executeByProc(
      "Product_GetIdListByKeyword",
      ProductModel.sequelize,
      [keyword, isActive]
    );
  }

  /**
   *
   * @param {Product} entity
   * @return {Promise<CustomResult>} result
   */
  async insert(entity) {
    /**
     * @type {CustomResult[]} result
     */
    const result = await DbExtension.executeByProc(
      "Product_Insert",
      ProductModel.sequelize,
      [
        entity.name,
        entity.unit_price,
        entity.have_promotion,
        entity.sale_price,
        entity.short_description,
        entity.description,
        entity.unit,
        entity.is_active,
        entity.publish_from,
        entity.publish_to,
        entity.created_by,
        entity.weight,
        entity.url_rewrite,
      ]
    );

    return result[0];
  }

  /**
   *
   * @param {Product} entity
   * @return {Promise<CustomResult>} result true or false
   */
  async update(entity) {
    /**
     * @type {CustomResult[]} result
     */
    const result = await DbExtension.executeByProc(
      "Product_Update",
      ProductModel.sequelize,
      [
        entity.id,
        entity.name,
        entity.unit_price,
        entity.have_promotion,
        entity.sale_price,
        entity.short_description,
        entity.description,
        entity.unit,
        entity.publish_from,
        entity.publish_to,
        entity.is_active,
        entity.weight,
        entity.url_rewrite,
      ]
    );

    return result[0];
  }

  /**
   *
   * @param {number} id
   * @return {Promise<boolean>} result true or false
   */
  async delete(id) {
    const result = await DbExtension.executeByProc(
      "Product_Delete",
      ProductModel.sequelize,
      [id]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = ProductRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/product').Product} Product
 * @typedef {import('../module/mysql/models/result').Result} CustomResult
 */
