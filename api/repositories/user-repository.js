const UserModel = require("../module/mysql/models/user");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");

class UserRepository {
  constructor() {
    this._rpUser = UserModel;
  }

  /**
   *
   * @param {number} id
   * @return user
   */
  async get(id) {
    return await this._rpUser.findOne({ where: { id: id } });
  }

  /**
   *
   * @param {string} username
   * @return {User} user
   */
  async getByUsername(username) {
    if ($.isEmpty(username) === true) {
      return null;
    }

    const lstUser = await DbExtension.executeByProc("User_GetByUsername", UserModel.sequelize, [
      username,
      true,
    ]);

    if($.isEmpty(lstUser) === true){
      return null;
    }

    return lstUser[0];
  }
}

module.exports = UserRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/user').User} User
 */