const ProductPhotoModel = require("../module/mysql/models/product-photo");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");

class ProductPhotoRepository {
  constructor() {
    this._rpProductPhoto = ProductPhotoModel;
  }

  /**
   * @param {number} id
   * @return {Promise<ProductPhoto[]>} result true or false
   */
  async getListByProductId(id) {
    return await DbExtension.executeByProc(
      "ProductPhoto_GetListByProductId",
      ProductPhotoModel.sequelize,
      [id]
    );
  }

  /**
   *
   * @param {ProductPhoto} entity
   * @return {Promise<boolean>} result true or false
   */
  async insert(entity) {
    const result = await DbExtension.executeByProc(
      "ProductPhoto_Insert",
      ProductPhotoModel.sequelize,
      [entity.product_id, entity.photo, entity.display_order]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {number} productId
   * @return {Promise<boolean>} result true or false
   */
  async deleteByProductId(productId) {
    const result = await DbExtension.executeByProc(
      "ProductPhoto_DeleteByProductId",
      ProductPhotoModel.sequelize,
      [productId]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = ProductPhotoRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/product-photo').ProductPhoto} ProductPhoto
 */
