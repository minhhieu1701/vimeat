const ProductCategoryModel = require("../module/mysql/models/product-category");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");

class ProductCategoryRepository {
  constructor() {
    this._rpProductCategory = ProductCategoryModel;
  }

  /**
   * @param {number} id
   * @return {Promise<ProductCategory[]>} result true or false
   */
  async getListByProductId(id) {
    return await DbExtension.executeByProc(
      "ProductCategory_GetListByProductId",
      ProductCategoryModel.sequelize,
      [id]
    );
  }

  /**
   *
   * @param {ProductCategory} entity
   * @return {Promise<boolean>} result true or false
   */
  async insert(entity) {
    const result = await DbExtension.executeByProc(
      "ProductCategory_Insert",
      ProductCategoryModel.sequelize,
      [entity.product_id, entity.category_id]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {number} productId
   * @return {Promise<boolean>} result true or false
   */
  async deleteByProductId(productId) {
    const result = await DbExtension.executeByProc(
      "ProductCategory_DeleteByProductId",
      ProductCategoryModel.sequelize,
      [productId]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {number} categoryId
   * @return {Promise<boolean>} result true or false
   */
  async deleteByCategoryId(categoryId) {
    const result = await DbExtension.executeByProc(
      "ProductCategory_DeleteByCategoryId",
      ProductCategoryModel.sequelize,
      [categoryId]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = ProductCategoryRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/product-category').ProductCategory} ProductCategory
 */
