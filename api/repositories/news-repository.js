const NewsModel = require("../module/mysql/models/news");
const DbExtension = require("../module/mysql/extension/database-extension");
const $ = require("lodash");
const sequelize = require("sequelize");

class NewsRepository {
  constructor() {
    this._rpNews = NewsModel;
  }

  /**
   *
   * @param {number} id
   * @param {boolean | null} isActive
   * @return {Promise<News>} news
   */
  async getByUid(uid, isActive = null) {
    const result = await DbExtension.executeByProc(
      "News_GetByUid",
      NewsModel.sequelize,
      [uid, isActive]
    );

    if (result && result.length > 0) {
      return result[0];
    }

    return null;
  }

  /**
   *
   * @param {boolean | null} isActive
   * @return {Promise<Array<string>>} news
   */
  async getUidList(isActive = true) {
    return await DbExtension.executeByProc(
      "News_GetUidList",
      NewsModel.sequelize,
      [isActive]
    );
  }

  /**
   *
   * @param {string} uids format: "abc,cde,efg"
   * @param {boolean | null} isActive
   * @return {Promise<Array<News>>} news
   */
  async getListByUids(uids, isActive = true) {
    return await DbExtension.executeByProc(
      "News_GetListByUids",
      NewsModel.sequelize,
      [uids, isActive]
    );
  }

  async getUidListByKeyword(keyword, isActive = true) {
    if ($.isEmpty(keyword) === true) {
      return null;
    }

    return await DbExtension.executeByProc(
      "News_GetUidListByKeyword",
      NewsModel.sequelize,
      [keyword, isActive]
    );
  }

  /**
   *
   * @param {News} entity
   * @return {Promise<boolean>} result true or false
   */
  async insert(entity) {
    const result = await DbExtension.executeByProc(
      "News_Insert",
      NewsModel.sequelize,
      [
        entity.uid,
        entity.title,
        entity.description,
        entity.publish_from,
        entity.publish_to,
        entity.photo,
        entity.is_active,
        entity.url_rewrite,
        entity.created_by
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {string} uid
   * @return {Promise<boolean>} result true or false
   */
  async increaseTotalView(uid) {
    const result = await DbExtension.executeByProc(
      "News_IncreaseTotalView",
      NewsModel.sequelize,
      [uid]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {News} entity
   * @return {Promise<boolean>} result true or false
   */
  async update(entity) {
    const result = await DbExtension.executeByProc(
      "News_Update",
      NewsModel.sequelize,
      [
        entity.uid,
        entity.title,
        entity.description,
        entity.publish_from,
        entity.publish_to,
        entity.is_active,
        entity.photo,
      ]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }

  /**
   *
   * @param {string} uid
   * @return {Promise<boolean>} result true or false
   */
  async delete(uid) {
    const result = await DbExtension.executeByProc(
      "News_Delete",
      NewsModel.sequelize,
      [uid]
    );

    if ($.isEmpty(result) === true || result[0].code !== 0) {
      return false;
    }

    return true;
  }
}

module.exports = NewsRepository;

//https://stackoverflow.com/questions/49836644/how-to-import-a-typedef-from-one-file-to-another-in-jsdoc-using-node-js
/**
 * @typedef {import('../module/mysql/models/news').News} News
 */
