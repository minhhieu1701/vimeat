Array.prototype.joinAsString = function (selectCallback) {
  const selectors = [];

  if (this.length < 1) {
    return "";
  }

  this.forEach((item) => {
    const selector = selectCallback(item);

    if (selector) {
      selectors.push(selector);
    }
  });

  if (selectors.length < 1) {
    return "";
  }

  return selectors.join(",");
};
