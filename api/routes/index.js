const fs = require('fs');

let routes = [];

const exceptedFiles = ['index.js', '_base.js'];

fs.readdirSync(__dirname)
    .filter(file => exceptedFiles.includes(file) === false)
    .forEach(file => {
        routes = routes.concat(require(`./${file}`))
    });

module.exports = routes;