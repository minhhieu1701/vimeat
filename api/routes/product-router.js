const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");
const Joi = require("joi");
const Manipulate = require("./_base");
const ProductBusiness = require("../business/product-business");
const responseModel = require("../models/shared/json-result").swaggerResponse;

module.exports = [
  {
    method: "GET",
    path: "/product/{id}",
    options: {
      auth: false,
      description: "Get product",
      notes: "Returns a product item by the id passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          id: Joi.string().required().description("the id for the todo item"),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || $.isEmpty(params.id) === true) {
              result.fail();
              return;
            }

            const entity = await ProductBusiness.get(params.id, true);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/product/{id}",
    options: {
      auth: "jwt",
      description: "Get product",
      notes: "Returns a product item by the id passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          id: Joi.string().required().description("the id for the todo item"),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || $.isEmpty(params.id) === true) {
              result.fail();
              return;
            }

            const entity = await ProductBusiness.get(params.id, null);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "GET",
    path: "/product/list",
    options: {
      auth: false,
      description: "Get product list",
      notes: "Returns a list of product item",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        query: Joi.object({
          requestCount: Joi.number().default(6).required(),
          lastUid: Joi.string().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { query } = request;

            if ($.isEmpty(query) === true) {
              result.fail();
              return;
            }

            let { requestCount, lastUid } = query;

            if ($.isEmpty(requestCount) === true || requestCount > 20) {
              requestCount = 20;
            }

            const response =
              (await ProductBusiness.get) - List(requestCount, lastUid);

            if ($.isEmpty(response) === true) {
              result.ok();
              return;
            }

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/product/search",
    options: {
      auth: "jwt",
      description: "Search product list for admin",
      notes: "Returns a list of product item",
      tags: ["api", "admin"], // ADD THIS TAG
      validate: {
        payload: Joi.object({
          requestCount: Joi.number().default(6).required(),
          page: Joi.number().default(1).required(),
          id: Joi.any(),
          keyword: Joi.any(),
          isActive: Joi.any().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.fail();
              return;
            }

            let { requestCount, page, id, keyword, isActive } = payload;

            if (!requestCount || requestCount > 20) {
              requestCount = 20;
            }

            const response = await ProductBusiness.getAdminList(
              requestCount,
              page,
              id,
              keyword,
              isActive
            );

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/product/insert",
    options: {
      auth: "jwt",
      description: "Create new product",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        payload: Joi.object({
          categories: Joi.array().required(),
          description: Joi.any(),
          havePromotion: Joi.boolean().required(),
          id: Joi.number().optional(),
          isActive: Joi.bool().required().default(false),
          name: Joi.string().required(),
          photo: Joi.array(),
          publishFrom: Joi.any().description("format: YYYY-MM-DD HH:mm"),
          publishTo: Joi.any().description("format: YYYY-MM-DD HH:mm"),
          salePrice: Joi.any().optional(),
          shortDescription: Joi.any(),
          unit: Joi.number().required(),
          unitPrice: Joi.number().required(),
          urlRewrite: Joi.any().required(),
          weight: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result, currentUserId) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.invalidParam();
              return;
            }

            const bIsSuccess = await ProductBusiness.insert(
              currentUserId,
              payload
            );

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/product/{id}/update",
    options: {
      auth: "jwt",
      description: "Update product",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.string().required(),
        }),
        payload: Joi.object({
          categories: Joi.array().required(),
          description: Joi.string().optional().allow("", null),
          havePromotion: Joi.boolean().required(),
          isActive: Joi.boolean().required().default(false),
          name: Joi.string().required().allow(""),
          photo: Joi.array().optional().allow(null),
          publishFrom: Joi.string()
            .allow("")
            .description("format: YYYY-MM-DD HH:mm"),
          publishTo: Joi.string()
            .allow("")
            .description("format: YYYY-MM-DD HH:mm"),
          salePrice: Joi.number().optional().allow(null),
          shortDescription: Joi.string().optional().allow("",null),
          unit: Joi.number().required(),
          unitPrice: Joi.number().required(),
          urlRewrite: Joi.string().required(),
          weight: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload, params } = request;

            if ($.isEmpty(payload) === true || $.isEmpty(params) === true) {
              result.invalidParam();
              return;
            }

            const bIsSuccess = await ProductBusiness.update(params.id, payload);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "DELETE",
    path: "/product/{id}/delete",
    options: {
      auth: "jwt",
      description: "Delete product",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.string().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true) {
              result.fail();
              return;
            }

            const bIsSuccess = await ProductBusiness.delete(params.id);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
];
