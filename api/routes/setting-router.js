const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");
const Joi = require("joi");
const Manipulate = require("./_base");
const SettingBusiness = require("../business/setting-business");

module.exports = [
  {
    method: "GET",
    path: "/setting/{id}",
    options: {
      auth: "jwt",
      description: "Get setting",
      notes: "Returns a setting item by the id passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || !params.id || params.id < 1) {
              result.invalidParam();
              return;
            }

            const entity = await SettingBusiness.get(params.id);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/setting/search",
    options: {
      auth: "jwt",
      description: "Search setting list for admin",
      notes: "Returns a list of setting item",
      tags: ["api", "admin"], // ADD THIS TAG
      validate: {
        payload: Joi.object({
          requestCount: Joi.number().default(6).required(),
          page: Joi.number().default(1).required(),
          id: Joi.any(),
          key: Joi.any(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.fail();
              return;
            }

            let { requestCount, page, id, key } = payload;

            if (!requestCount || requestCount > 20) {
              requestCount = 20;
            }

            const response = await SettingBusiness.getList(
              requestCount,
              page,
              id,
              key
            );

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/setting/insert",
    options: {
      auth: "jwt",
      description: "Create new setting",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        payload: Joi.object({
          key: Joi.string().max(500).required(),
          value: Joi.string().max(500).required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await SettingBusiness.insert(payload);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/setting/{id}/update",
    options: {
      auth: "jwt",
      description: "Update setting",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
        payload: Joi.object({
          value: Joi.string().max(500).required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload, params } = request;

            if ($.isEmpty(payload) === true || $.isEmpty(params) === true) {
              result.invalidParam();
              return;
            }

            const bIsSuccess = await SettingBusiness.update(params.id, payload.value);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "DELETE",
    path: "/setting/{id}/delete",
    options: {
      auth: "jwt",
      description: "Delete setting",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await SettingBusiness.delete(params.id);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
];
