const Func = require("../module/util/function");
const JsonResult = require("../models/shared/json-result");
const UserModel = require("../module/mysql/models/user");
const actionResultCode = require("../constant/action-result-code");

/**
 * @param {Object} request
 * @param {Manipulate} callback
 * @param {boolean} isRequireLogin
 * @returns {Promise<JsonResult, number | null>} JsonResult
 */
const Manipulate = async function (request, callback, isRequireLogin = false) {
  const jsonResult = new JsonResult();
  let currentUserId = null;

  if (request && request.auth && request.auth.credentials) {
    const user = await UserModel.findOne({
      where: {
        uid: request.auth.credentials.id,
      },
    });

    if(user){
      currentUserId = user.id;
    }
  }

  try {
    if (Func.isAsyncFunction(callback) === true) {
      await callback(jsonResult, currentUserId);
    } else {
      callback(jsonResult, currentUserId);
    }
  } catch (error) {
    console.log(error);
    jsonResult.fail();
  }

  return jsonResult;
};

module.exports = Manipulate;

/**
 * @callback Manipulate
 * @param {JsonResult} Json result
 * @param {number} currentUserId currentUserId
 * @returns {JsonResult} response
 */
