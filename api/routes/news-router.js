const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");
const Joi = require("joi");
const Manipulate = require("./_base");
const NewsBusiness = require("../business/news-business");
const responseModel = require("../models/shared/json-result").swaggerResponse;

module.exports = [
  {
    method: "GET",
    path: "/news/{uid}",
    options: {
      auth: false,
      description: "Get news",
      notes: "Returns a news item by the uid passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          uid: Joi.string().required().description("the uid for the todo item"),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || $.isEmpty(params.uid) === true) {
              result.fail();
              return;
            }

            const entity = await NewsBusiness.getByUid(params.uid);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/news/{uid}",
    options: {
      auth: "jwt",
      description: "Get news",
      notes: "Returns a news item by the uid passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          uid: Joi.string().required().description("the uid for the todo item"),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || $.isEmpty(params.uid) === true) {
              result.fail();
              return;
            }

            const entity = await NewsBusiness.getByUid(params.uid, true);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "GET",
    path: "/news/list",
    options: {
      auth: false,
      description: "Get news list",
      notes: "Returns a list of news item",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        query: Joi.object({
          requestCount: Joi.number().default(6).required(),
          lastUid: Joi.string().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { query } = request;

            if ($.isEmpty(query) === true) {
              result.fail();
              return;
            }

            let { requestCount, lastUid } = query;

            if ($.isEmpty(requestCount) === true || requestCount > 20) {
              requestCount = 20;
            }

            const response = await NewsBusiness.getList(requestCount, lastUid);

            if ($.isEmpty(response) === true) {
              result.fail();
              return;
            }

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/news/search",
    options: {
      auth: "jwt",
      description: "Search news list for admin",
      notes: "Returns a list of news item",
      tags: ["api", "admin"], // ADD THIS TAG
      validate: {
        payload: Joi.object({
          requestCount: Joi.number().default(6).required(),
          page: Joi.number().default(1).required(),
          uid: Joi.any(),
          keyword: Joi.any(),
          isActive: Joi.any().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.fail();
              return;
            }

            let { requestCount, page, uid, keyword, isActive } = payload;

            if (!requestCount || requestCount > 20) {
              requestCount = 20;
            }

            const response = await NewsBusiness.getAdminList(
              requestCount,
              page,
              uid,
              keyword,
              isActive
            );

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/news/insert",
    options: {
      auth: "jwt",
      description: "Create new news",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        payload: Joi.object({
          title: Joi.string().max(500).required(),
          description: Joi.any(),
          publishFrom: Joi.any()
            .description("format: YYYY-MM-DD HH:mm")
            .required(),
          isActive: Joi.bool().required().default(false),
          publishTo: Joi.any().description("format: YYYY-MM-DD HH:mm"),
          photo: Joi.any(),
          urlRewrite: Joi.any().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result, currentUserId) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await NewsBusiness.insert(payload, currentUserId);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/news/{uid}/update",
    options: {
      auth: "jwt",
      description: "Update news",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          uid: Joi.string().required(),
        }),
        payload: Joi.object({
          title: Joi.string().max(500).required(),
          description: Joi.any(),
          publishFrom: Joi.any()
            .description("format: YYYY-MM-DD HH:mm")
            .required(),
          publishTo: Joi.any().description("format: YYYY-MM-DD HH:mm"),
          isActive: Joi.bool().required().default(false),
          photo: Joi.any(),
          urlRewrite: Joi.string().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload, params } = request;

            if ($.isEmpty(payload) === true || $.isEmpty(params) === true) {
              result.fail();
              return;
            }

            const bIsSuccess = await NewsBusiness.update(params.uid, payload);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "DELETE",
    path: "/news/{uid}/delete",
    options: {
      auth: "jwt",
      description: "Delete news",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          uid: Joi.string().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await NewsBusiness.delete(params.uid);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
];
