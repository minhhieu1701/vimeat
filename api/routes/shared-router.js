const Joi = require("joi");
const Manipulate = require("./_base");
const ImageBusiness = require("../business/image-business");
const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");

const _bizImage = new ImageBusiness();

module.exports = [
  {
    method: "POST",
    path: "/upload/image",
    options: {
      auth: "jwt",
      description: "Upload only image",
      notes: "Only with jpg, jpeg and png",
      tags: ["api"], // ADD THIS TAG
      response: {
        schema: Joi.object().keys({
          code: Joi.number().example(0),
          message: Joi.string().example("Success"),
          data: Joi.any(),
        }),
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "form",
        },
      },
      validate: {
        payload: Joi.object({
          folderName: Joi.string().required(),
          file: Joi.any()
            .required()
            .meta({ swaggerType: "file" })
            .description("image file"),
        }),
      },
      payload: {
        maxBytes: 1048576,
        parse: true,
        output: "stream",
        multipart: true,
        allow: "multipart/form-data",
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const resp = await _bizImage.saveImage(
              [request.payload["file"]],
              request.payload["folderName"]
            );

            result.ok(resp);
          },
          true
        );
      },
    },
  },
  {
    method: "GET",
    path: "/image/{folderName}/{date}/{size}/{fileName}",
    options: {
      auth: false,
      description: "Upload only image",
      notes: "Only with jpg, jpeg and png",
      tags: ["api", "admin"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "form",
        },
      },
      cache: {
        expiresIn: 30 * 1000,
        privacy: "private",
      },
      validate: {
        params: Joi.object({
          folderName: Joi.string().required(),
          fileName: Joi.string().required(),
          date: Joi.string().required(),
          size: Joi.string().optional(),
        }),
      },
      handler: async (request, reply) => {
        const { params } = request;

        if ($.isEmpty(params) === true) {
          reply(null);
          return;
        }

        const { fileName, date, size, folderName } = params;

        if (
          $.isEmpty(fileName) === true ||
          $.isEmpty(date) === true ||
          $.isEmpty(size) === true ||
          $.isEmpty(folderName) === true
        ) {
          reply(null);
        }

        let _size = [];

        if ($.isEqual(size, "s") === true) {
          _size.push([0, 0]);
        } else {
          _size = size.split("x").map((x) => +x);
        }

        const buffer = await _bizImage.get(date, fileName, _size, folderName);

        const response = reply.response(buffer);
        response.type("image/png");
        response.header("Cache-Control", "max-age=86400");
        return response;
      },
    },
  },
  {
    method: "GET",
    path: "/image/{fileName}",
    options: {
      auth: false,
      description: "No image",
      notes: "Get no image",
      tags: ["api", "admin"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "form",
        },
      },
      cache: {
        expiresIn: 30 * 1000,
        privacy: "private",
      },
      validate: {
        params: Joi.object({
          fileName: Joi.string().required(),
        }),
      },
      handler: async (request, reply) => {
        const { params } = request;

        if ($.isEmpty(params) === true) {
          reply(null);
          return;
        }

        const { fileName } = params;

        if ($.isEmpty(fileName) === true) {
          reply(null);
        }

        const buffer = await _bizImage.getDefaultImage(fileName);

        const response = reply.response(buffer);
        response.type("image/png");
        response.header("Cache-Control", "max-age=86400");
        return response;
      },
    },
  },
];
