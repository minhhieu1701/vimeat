const Joi = require("joi");
const responseModel = require("../models/shared/json-result").swaggerResponse;
const AuthBusiness = require("../business/auth-business");
const Manipulate = require("./_base");
const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");

module.exports = [
  {
    method: "POST",
    path: "/auth",
    options: {
      auth: false,
      // auth: "jwt",
      description: "Authenticate account",
      notes: "Return the token for your login session",
      tags: ["api"], // ADD THIS TAG
      response: {
        schema: responseModel,
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        payload: Joi.object({
          username: Joi.string().required().description("username"),
          password: Joi.string().required().description("password"),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const { username, password } = payload;

            if ($.isEmpty(username) === true || $.isEmpty(password) === true) {
              result.bind(actionResultCode.FORBIDDEN);
              return;
            }

            const resp = await AuthBusiness.Login({
              username: username,
              password: password,
            });

            if (
              $.isEmpty(resp) === false &&
              resp.resultCode.value === actionResultCode.SUCCESS.value
            ) {
              result.ok(resp.authentication);

              return;
            }

            result.bind(resp.resultCode);
          },
          false
        );
      },
    },
  },
];
