const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");
const Joi = require("joi");
const Manipulate = require("./_base");
const CategoryBusiness = require("../business/category-business");

module.exports = [
  {
    method: "GET",
    path: "/category/{id}",
    options: {
      auth: "jwt",
      description: "Get category",
      notes: "Returns a category item by the id passed in the path",
      tags: ["api", "v1"], // ADD THIS TAG
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true || !params.id || params.id < 1) {
              result.invalidParam();
              return;
            }

            const entity = await CategoryBusiness.get(params.id);

            if ($.isEmpty(entity) === true) {
              result.ok();
              return;
            }

            result.ok(entity);
          },
          false
        );
      },
    },
  },
  {
    method: "GET",
    path: "/category/enum/list",
    options: {
      auth: false,
      description: "Get category list as enum item",
      notes: "Returns a category list as enum list",
      tags: ["api", "v1"], // ADD THIS TAG
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const lst = await CategoryBusiness.getListAsEnumList();

            if ($.isEmpty(lst) === true) {
              result.ok();
              return;
            }

            result.ok(lst);
          },
          false
        );
      },
    },
  },
  {
    method: "POST",
    path: "/category/search",
    options: {
      auth: "jwt",
      description: "Search category list for admin",
      notes: "Returns a list of category item",
      tags: ["api", "admin"], // ADD THIS TAG
      validate: {
        payload: Joi.object({
          requestCount: Joi.number().default(6).required(),
          page: Joi.number().default(1).required(),
          id: Joi.any(),
          keyword: Joi.any(),
          isActive: Joi.any().optional(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.fail();
              return;
            }

            let { requestCount, page, id, keyword, isActive } = payload;

            if (!requestCount || requestCount > 20) {
              requestCount = 20;
            }

            const response = await CategoryBusiness.getList(
              requestCount,
              page,
              id,
              keyword,
              isActive
            );

            result.ok(response);
          },
          false
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/category/insert",
    options: {
      auth: "jwt",
      description: "Create new category",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        payload: Joi.object({
          name: Joi.string().max(500).required(),
          isActive: Joi.bool().required().default(false),
          photo: Joi.any(),
          background: Joi.string(),
          borderColor: Joi.string()
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload } = request;

            if ($.isEmpty(payload) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await CategoryBusiness.insert(payload);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "PUT",
    path: "/category/{id}/update",
    options: {
      auth: "jwt",
      description: "Update category",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
        payload: Joi.object({
          name: Joi.string().max(500).required(),
          isActive: Joi.bool().required().default(false),
          photo: Joi.any(),
          background: Joi.string(),
          borderColor: Joi.string()
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { payload, params } = request;

            if ($.isEmpty(payload) === true || $.isEmpty(params) === true) {
              result.invalidParam();
              return;
            }

            const bIsSuccess = await CategoryBusiness.update(
              params.id,
              payload
            );

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
  {
    method: "DELETE",
    path: "/category/{id}/delete",
    options: {
      auth: "jwt",
      description: "Delete category",
      notes: "Returns true or false",
      tags: ["api", "v1"], // ADD THIS TAG
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Success",
            },
          },
          payloadType: "json",
        },
      },
      validate: {
        params: Joi.object({
          id: Joi.number().required(),
        }),
      },
      handler: async (request, reply) => {
        return await Manipulate(
          request,
          async (result) => {
            const { params } = request;

            if ($.isEmpty(params) === true) {
              result.bind(actionResultCode.FAIL);
              return;
            }

            const bIsSuccess = await CategoryBusiness.delete(params.id);

            if (bIsSuccess === false) {
              result.fail();
              return;
            }

            result.ok();
          },
          true
        );
      },
    },
  },
];
