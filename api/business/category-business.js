const { getCategoryPhotoUrl } = require("../module/util/media");
const { Partition, GetPageSize } = require("../module/util/paging");
const $ = require("lodash");
const MEDIA_CONFIG = require("../media-config");
const Property = require("../module/util/property");
const rpCategory = require("../repositories/category-repository");
const rpProductCategory = require("../repositories/product-category-repository");

class CategoryBusiness {
  constructor() {
    this._rpCategory = new rpCategory();
    this._rpProductCategory = new rpProductCategory();
  }

  sizes = MEDIA_CONFIG.category.crop;

  /**
   * @param {InsertRequest} model category model
   * @returns {Promise<boolean>} response
   */
  async insert(model) {
    if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.name) === true) {
      return false;
    }

    /** @type {ResponseItem} */
    const entity = {
      is_active: model.isActive,
      name: model.name,
      photo: model.photo || null,
      background: model.background || null,
      border_color: model.borderColor || null,
    };

    return await this._rpCategory.insert(entity);
  }

  /**
   * @param {number} id id
   * @param {UpdateRequest} model category model
   * @returns {Promise<boolean>} response
   */
  async update(id, model) {
    if (!id || id < 1) {
      return false;
    } else if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.name) === true) {
      return false;
    }

    /**
     * @type {ResponseItem}
     */
    const entity = {
      id: id,
      is_active: model.isActive,
      name: model.name,
      photo: model.photo || null,
      background: model.background || null,
      border_color: model.borderColor || null,
    };

    return await this._rpCategory.update(entity);
  }

  /**
   * @param {number} id uid
   * @returns {Promise<boolean>} response
   */
  async delete(id) {
    if (!id || id < 1) {
      return false;
    }

    await this._rpProductCategory.deleteByCategoryId(id);

    return await this._rpCategory.delete(id);
  }

  /**
   * @param {number} id id
   * @returns {Promise<ResponseItem>} response
   */
  async get(id) {
    if (!id || id < 1) {
      return false;
    }

    const entity = await this._rpCategory.get(id, null);

    const model = {
      id: entity.id,
      name: entity.name,
      background: entity.background,
      borderColor: entity.border_color,
      isActive: Property.bindBoolean(entity.is_active),
    };

    if ($.isEmpty(entity.photo) === false) {
      model.photo = getCategoryPhotoUrl(entity.photo, this.sizes);
    }

    return model;
  }

  /**
   * @param {number} requestCount request count
   * @param {number} requestPageNumber page number
   * @param {string} uid
   * @param {string} keyword
   * @param {boolean} isActive
   * @returns {Promise<{
   *    totalPage: number
   *    records: any[]
   * }>} response
   */
  async getList(
    requestCount,
    requestPageNumber,
    id = null,
    keyword = null,
    isActive = null
  ) {
    let lstId;
    let result = {
      totalPage: 0,
      totalItem: 0,
      records: [],
    };

    if (id && id > 0) {
      lstId = [
        {
          id,
        },
      ];
    } else if ($.isEmpty(keyword) === false) {
      //search by name
    } else {
      lstId = await this._rpCategory.getIdList(isActive);
    }

    if ($.isEmpty(lstId) === true) {
      return [];
    }

    const iTotalItem = lstId.length;
    const partitionUid = [];

    const iTotalPage = GetPageSize(requestCount, iTotalItem);

    const { startIndex, endIndex } = Partition(
      requestCount,
      requestPageNumber,
      iTotalItem
    );

    for (let index = startIndex; index < endIndex; index++) {
      partitionUid.push(lstId[index].id);
    }

    if (!partitionUid || partitionUid.length < 1) {
      return [];
    }

    const lstCategory = await this._rpCategory.getListByIds(
      partitionUid.join(","),
      null
    );

    if ($.isEmpty(lstCategory) === true) {
      return [];
    }

    result = {
      totalPage: iTotalPage,
      totalItem: iTotalItem,
      records: [],
    };

    lstCategory.forEach((category) => {
      const x = {
        id: category.id,
        name: category.name,
        isActive: Property.bindBoolean(category.is_active),
      };

      if ($.isEmpty(category.photo) === false) {
        x.photo = getCategoryPhotoUrl(category.photo, this.sizes);
      }

      result.records.push(x);
    });

    return result;
  }

  /**
   * @param {boolean} isActive
   * @returns {Promise<Array<EnumItem>>} response
   */
  async getListAsEnumList() {
    const lstCategory = await this._rpCategory.getList(true);

    /** @type {EnumItem[]} */
    const result = [];

    if ($.isEmpty(lstCategory) === true) {
      return result;
    }

    lstCategory.forEach((category) => {
      result.push({
        text: category.name,
        value: category.id,
      });
    });

    return result;
  }
}

module.exports = new CategoryBusiness();

/**
 * Declare our returned type
 * @typedef {import("../module/mysql/models/category").Category} ResponseItem
 * /**
 * @typedef {import("../constant/action-result-code").ResultCode} ResultCode
 *
 * @typedef {Object} InsertRequest
 * @property {string} name
 * @property {boolean} isActive
 * @property {string} photo
 * @property {string} background
 * @property {string} borderColor
 *
 *
 * @typedef {Object} UpdateRequest
 * @property {number} id
 * @property {string} name
 * @property {boolean} isActive
 * @property {string} photo
 * @property {string} background
 * @property {string} borderColor
 *
 * @typedef {import("../models/shared/get-list").GetList} GetList
 *
 * @typedef {import("../models/shared/enum-item").EnumItem} EnumItem
 *
 */
