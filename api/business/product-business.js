const { getProductPhotoUrl } = require("../module/util/media");
const { Partition, GetPageSize } = require("../module/util/paging");
const $ = require("lodash");
const MEDIA_CONFIG = require("../media-config");
const Property = require("../module/util/property");
const rpProduct = require("../repositories/product-repository");
const rpCategory = require("../repositories/category-repository");
const rpProductPhoto = require("../repositories/product-photo-repository");
const rpProductCategory = require("../repositories/product-category-repository");

class ProductBusiness {
  constructor() {
    this._rpCategory = new rpCategory();
    this._rpProduct = new rpProduct();
    this._rpProductCategory = new rpProductCategory();
    this._rpProductPhoto = new rpProductPhoto();
  }

  sizes = MEDIA_CONFIG.product.crop;

  /**
   * @param {number} currentUserId current user id
   * @param {InsertRequest} model product model
   * @returns {Promise<boolean>} response
   */
  async insert(currentUserId, model) {
    if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.name) === true) {
      return false;
    } else if (!model.unitPrice || model.unitPrice < 1) {
      return false;
    } else if (!model.unit || model.unit < 1) {
      return false;
    }

    /** @type {ResponseItem} */
    const entity = {
      created_by: currentUserId,
      description: model.description,
      have_promotion: model.havePromotion,
      is_active: model.isActive,
      name: model.name,
      sale_price: model.salePrice,
      short_description: model.shortDescription,
      unit: model.unit,
      unit_price: model.unitPrice,
      publish_from: model.publishFrom,
      publish_to: model.publishTo,
      weight: model.weight,
      url_rewrite: model.urlRewrite,
    };

    let result = await this._rpProduct.insert(entity);

    if (!result || result.code !== 0) {
      return false;
    }

    if ($.isEmpty(model.photo) === true) {
      return true;
    }

    const productId = +result.data;

    for (let index = 0, length = model.photo.length; index < length; index++) {
      const resp = await this._rpProductPhoto.insert({
        product_id: productId,
        photo: model.photo[index],
        display_order: index,
      });
    }

    if ($.isEmpty(model.categories) === true) {
      return true;
    }

    for (
      let index = 0, length = model.categories.length;
      index < length;
      index++
    ) {
      const resp = await this._rpProductCategory.insert({
        product_id: productId,
        category_id: model.categories[index],
      });
    }

    return true;
  }

  /**
   * @param {number} id id
   * @param {UpdateRequest} model product model
   * @returns {Promise<boolean>} response
   */
  async update(id, model) {
    if (!id || id < 1) {
      return false;
    } else if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.name) === true) {
      return false;
    } else if (!model.unitPrice || model.unitPrice < 1) {
      return false;
    } else if (!model.unit || model.unit < 1) {
      return false;
    }

    /**
     * @type {ResponseItem}
     */
    const entity = {
      description: model.description,
      have_promotion: model.havePromotion,
      id: id,
      is_active: model.isActive,
      name: model.name,
      publish_from: model.publishFrom,
      publish_to: model.publishTo,
      sale_price: model.salePrice,
      short_description: model.shortDescription,
      unit_price: model.unitPrice,
      unit: model.unit,
      url_rewrite: model.urlRewrite,
      weight: model.weight,
    };

    const result = await this._rpProduct.update(entity);

    if (!result || result.code !== 0) {
      return false;
    }

    const lstProductPhoto = await this._rpProductPhoto.getListByProductId(id);

    if ($.isEmpty(lstProductPhoto) === false) {
      const bIsSuccess = await this._rpProductPhoto.deleteByProductId(id);

      if (bIsSuccess === false) {
        return true;
      }
    }

    if ($.isEmpty(model.photo) === true) {
      return true;
    }

    for (let index = 0, length = model.photo.length; index < length; index++) {
      const resp = await this._rpProductPhoto.insert({
        product_id: id,
        photo: model.photo[index],
        display_order: index,
      });
    }

    if ($.isEmpty(model.categories) === true) {
      return true;
    }

    const lstProductCategory = await this._rpProductCategory.getListByProductId(
      id
    );

    if ($.isEmpty(lstProductCategory) === false) {
      const bIsSuccess = await this._rpProductCategory.deleteByProductId(id);

      if (bIsSuccess === false) {
        return true;
      }
    }

    for (
      let index = 0, length = model.categories.length;
      index < length;
      index++
    ) {
      const resp = await this._rpProductCategory.insert({
        product_id: id,
        category_id: model.categories[index],
      });
    }

    return true;
  }

  /**
   * @param {number} id uid
   * @returns {Promise<boolean>} response
   */
  async delete(id) {
    if (!id || id < 1) {
      return false;
    }

    await this._rpProductPhoto.deleteByProductId(id);

    await this._rpProductCategory.deleteByProductId(id);

    return await this._rpProduct.delete(id);
  }

  /**
   * @param {number} id id
   * @param {boolean} isActive is active
   * @returns {Promise<Detail>} response
   */
  async get(id, isActive = null) {
    if (!id || id < 1) {
      return false;
    }

    const entity = await this._rpProduct.get(id, isActive);

    /**
     * @type {Detail}
     */
    const model = {
      createdAt: entity.created_at.getTime().toString(),
      createdBy: entity.created_by,
      description: entity.description,
      havePromotion: Property.bindBoolean(entity.have_promotion),
      id: entity.id,
      isActive: Property.bindBoolean(entity.is_active),
      name: entity.name,
      salePrice: entity.sale_price,
      shortDescription: entity.short_description,
      unit: entity.unit,
      unitPrice: entity.unit_price,
      urlRewrite: entity.url_rewrite,
      weight: entity.weight,
    };

    if (entity.publish_from) {
      model.publishFrom = entity.publish_from.getTime().toString();
    }

    if (entity.publish_to) {
      model.publishTo = entity.publish_to.getTime().toString();
    }

    const lstPhoto = await this._rpProductPhoto.getListByProductId(id);

    if ($.isEmpty(lstPhoto) === true) {
      return model;
    }

    model.photos = [];

    lstPhoto.forEach((item) => {
      model.photos.push(getProductPhotoUrl(item.photo, this.sizes));
    });

    const lstProductCategory = await this._rpProductCategory.getListByProductId(
      id
    );

    if ($.isEmpty(lstProductCategory) === true) {
      return model;
    }

    const lstCategory = await this._rpCategory.getListByIds(
      lstProductCategory.joinAsString((x) => x.category_id),
      true
    );

    if ($.isEmpty(lstCategory) === true) {
      return model;
    }

    model.categories = lstCategory.map(x => x.id);

    return model;
  }

  /**
   * @param {number} requestCount request count
   * @param {number} requestPageNumber page number
   * @param {string} uid
   * @param {string} keyword
   * @param {boolean} isActive
   * @returns {Promise<{
   *    totalPage: number
   *    records: any[]
   * }>} response
   */
  async getAdminList(
    requestCount,
    requestPageNumber,
    id = null,
    keyword = null,
    isActive = null
  ) {
    let lstId;
    let result = {
      totalPage: 0,
      totalItem: 0,
      records: [],
    };

    if (id && id > 0) {
      lstId = [
        {
          id,
        },
      ];
    } else if ($.isEmpty(keyword) === false) {
      //search by name
      lstId = await this._rpProduct.getIdListByKeyword(keyword, isActive);
    } else {
      lstId = await this._rpProduct.getIdList(isActive);
    }

    if ($.isEmpty(lstId) === true) {
      return [];
    }

    const iTotalItem = lstId.length;
    const partitionId = [];

    const iTotalPage = GetPageSize(requestCount, iTotalItem);

    const { startIndex, endIndex } = Partition(
      requestCount,
      requestPageNumber,
      iTotalItem
    );

    for (let index = startIndex; index < endIndex; index++) {
      partitionId.push(lstId[index].id);
    }

    if (!partitionId || partitionId.length < 1) {
      return [];
    }

    const lstProduct = await this._rpProduct.getListByIds(
      partitionId.join(","),
      null
    );

    if ($.isEmpty(lstProduct) === true) {
      return [];
    }

    result = {
      totalPage: iTotalPage,
      totalItem: iTotalItem,
      records: [],
    };

    lstProduct.forEach((product) => {
      const x = {
        id: product.id,
        name: product.name,
        description: product.description,
        shortDescription: product.short_description,
        unitPrice: product.unit_price,
        havePromotion: Property.bindBoolean(product.have_promotion),
        salePrice: product.sale_price,
        weight: product.weight,
        unit: product.unit,
        isActive: Property.bindBoolean(product.is_active),
        publishFrom: product.publish_from
          ? new Date(product.publish_from).getTime()
          : null,
        publishTo: product.publish_to
          ? new Date(product.publish_to).getTime()
          : null,
        createdAt: product.created_at.getTime(),
      };

      if ($.isEmpty(product.photo) === false) {
        x.photo = getProductPhotoUrl(product.photo, this.sizes);
      }

      result.records.push(x);
    });

    return result;
  }

  /**
   * @param {number} requestCount request count
   * @param {string | null} lastId last id of previous request
   * @returns {Promise<GetList>} response
   */
  async getList(requestCount, lastId = null) {
    const lstId = await this._rpProduct.getIdList(true);

    if ($.isEmpty(lstId) === true) {
      return [];
    }

    let idx = 0;
    let iTotalRemain = 0;
    const partitionId = [];

    if (lstId.length > requestCount) {
      iTotalRemain = lstId.length;
    }

    if ($.isEmpty(lastUid) === false) {
      lstId.forEach((uid) => {
        idx++;

        iTotalRemain--;

        if ($.isEqual(uid, lastUid) === true) {
          return;
        }
      });
    }

    for (let i = 0, j = lstId.length; idx < j; idx++) {
      if (i === requestCount) {
        break;
      }

      partitionId.push(lstId[idx].uid);

      i++;
    }

    if (partitionId.length < 1) {
      return [];
    }

    const lstProduct = await this._rpProduct.getListByIds(
      partitionId.join(","),
      true
    );

    if ($.isEmpty(lstProduct) === true) {
      return [];
    }

    /**
     * @type {GetList}
     */
    result = {
      totalPage: iTotalPage,
      totalItem: iTotalItem,
      records: [],
    };

    lstProduct.forEach((product) => {
      const x = {
        id: product.id,
        name: product.name,
        description: product.description,
        shortDescription: product.shortDescription,
        unitPrice: product.unitPrice,
        havePromotion: product.havePromotion,
        salePrice: product.salePrice,
        weight: product.weight,
        unit: product.unit,
        isActive: Property.bindBoolean(product.is_active),
        publishFrom: product.publish_from
          ? new Date(product.publish_from).getTime()
          : null,
        publishTo: product.publish_to
          ? new Date(product.publish_to).getTime()
          : null,
      };

      if ($.isEmpty(product.photo) === false) {
        x.photo = getProductPhotoUrl(product.photo, this.sizes);
      }

      result.records.push(x);
    });

    return result;
  }
}

module.exports = new ProductBusiness();

/**
 * Declare our returned type
 * @typedef {import("../module/mysql/models/product").Product} ResponseItem
 * /**
 * @typedef {import("../constant/action-result-code").ResultCode} ResultCode
 *
 * @typedef {Object} InsertRequest
 * @property {boolean} havePromotion
 * @property {boolean} isActive
 * @property {number | null} salePrice
 * @property {number} createdBy
 * @property {number} unit
 * @property {number} unitPrice
 * @property {number} weight
 * @property {string | null} publishFrom
 * @property {string | null} publishTo
 * @property {string[]} photo
 * @property {string} description
 * @property {string} name
 * @property {string} shortDescription
 * @property {string} urlRewrite
 *
 *
 * @typedef {Object} UpdateRequest
 * @property {boolean} havePromotion
 * @property {boolean} isActive
 * @property {number | null} salePrice
 * @property {number} createdBy
 * @property {number} id
 * @property {number} unit
 * @property {number} unitPrice
 * @property {number} weight
 * @property {string | null} publishFrom
 * @property {string | null} publishTo
 * @property {string[]} photo
 * @property {string} description
 * @property {string} name
 * @property {string} shortDescription
 * @property {string} urlRewrite
 *
 * @typedef {import("../models/shared/get-list").GetList} GetList
 *
 * @typedef {import("../models/product/detail").Detail} Detail
 *
 */
