const _array = require("../module/util/array");
const _path = require("path");
const $ = require("lodash");
const fs = require("fs");
const MEDIA_CONFIG = require("../media-config");
const sharp = require("sharp");
const sizeOf = require("image-size");
const StringBuffer = require("../module/util/string-buffer");

const PhotoItem = require("../models/shared/photo-item");

const WORKING_DIR = process.cwd();
const DOMAIN = `http://${process.env.HOST}${
  process.env.PORT ? `:${process.env.PORT}` : ""
}`;
const NO_IMAGE_PATH = `${WORKING_DIR}\\uploads\\no-image.jpg`;
const POSITION_TO_START_CROP = {
  left: 0, //crop from left percentage
  top: 0.5, //crop from top percentage
  size: 1, //size percentage (extracted from width)
};

class ImageBusiness {
  constructor() {}

  /**
   * @param {any[]} files List image files
   * @returns {Promise<[{
   *    isSuccess: boolean
   *    fileName: string
   *    url: string
   * }]>} response
   */
  async saveImage(files, folderName) {
    return new Promise((resolve, reject) => {
      const result = [];

      if ($.isEmpty(files) === true) {
        resolve([
          {
            isSuccess: false,
          },
        ]);
      }

      const now = new Date();

      const sb = new StringBuffer();

      sb.append(now.getMonth() + 1)
        .append("_")
        .append(now.getFullYear());

      const shortDate = sb.toString();

      const uploadPath = `${WORKING_DIR}\\uploads\\${folderName}`;

      if (fs.existsSync(uploadPath + `\\${shortDate}`) === false) {
        fs.mkdirSync(uploadPath);
        fs.mkdirSync(uploadPath + `\\${shortDate}`);
      }

      for (let i = 0, j = files.length; i < j; i++) {
        const image = files[i];
        let fileName = `${shortDate}\\image_${new Date().getTime()}${_path.extname(
          image.hapi.filename
        )}`;
        const fileName1 = `${shortDate}/s/image_${new Date().getTime()}${_path.extname(
          image.hapi.filename
        )}`;
        let path = `${uploadPath}\\${fileName}`;
        const url = `${DOMAIN}/image/${folderName}/${fileName1}`;

        try {
          fs.writeFileSync(path, image._data);

          fileName = fileName.replace('\\', '/');

          result.push({
            isSuccess: true,
            fileName: fileName,
            url: url,
          });
        } catch (err) {
          result.push({
            isSuccess: false,
            fileName: fileName,
            url: url,
            description: err,
          });
        }
      }

      resolve(result);
    });
  }

  /**
   * @param {string} shortDate
   * @param {string} fileName
   * @param {Array<number>} size
   * @param {string} folderName
   */
  async get(shortDate, fileName, size, folderName) {
    if (
      $.isEmpty(size) === true ||
      $.isEmpty(fileName) === true ||
      $.isEmpty(shortDate) === true ||
      $.isEmpty(folderName) === true
    ) {
      return null;
    }

    let path = `${WORKING_DIR}\\uploads\\${folderName}\\${shortDate}\\${fileName}`;

    const bIsResize = isResize(size);

    if ($.isEmpty(size) === false && bIsResize === false) {
      path = await this.crop(shortDate, fileName, size, folderName);
    } else {
      path = await this.resize(shortDate, fileName, size, folderName);
    }

    let buffer;
    try {
      buffer = fs.readFileSync(path);
    } catch (error) {
      buffer = fs.readFileSync(NO_IMAGE_PATH);
    }

    return buffer;
  }

  async getDefaultImage(fileName){
    const path = `${WORKING_DIR}\\uploads\\${fileName}`;

    let buffer;
    try {
      buffer = fs.readFileSync(path);
    } catch (error) {
      buffer = fs.readFileSync(NO_IMAGE_PATH);
    }

    return buffer;
  }

  /**
   * @param {string} path
   * @param {Array<number>} size Size which you want to resize, [width, height]
   */
  async resize(shortDate, fileName, size, folderName) {
    if (
      $.isEmpty(size) === true ||
      $.isEmpty(fileName) === true ||
      $.isEmpty(shortDate) === true ||
      $.isEmpty(folderName) === true
    ) {
      return null;
    }

    const originalPath = `${WORKING_DIR}\\uploads\\${folderName}\\${shortDate}\\${fileName}`;

    const definedSize = MEDIA_CONFIG[folderName];

    if ($.isEmpty(definedSize) === true) {
      throw new Error("invalid folder");
    }

    const closestWidth = _array.getClosestValue(
      MEDIA_CONFIG[folderName].resize.map((x) => x[0]),
      size[0]
    );

    const uploadPath = `${WORKING_DIR}\\uploads\\${folderName}\\${shortDate}\\${closestWidth}`;
    const resizedPath = `${uploadPath}\\${fileName}`;

    if (fs.existsSync(uploadPath) === false) {
      fs.mkdirSync(uploadPath);
    }

    if (fs.existsSync(resizedPath) === true) {
      return resizedPath;
    }

    try {
      const resized = await sharp(originalPath)
        .resize(closestWidth, null, {
          kernel: sharp.kernel.nearest,
          fit: "contain",
          position: "center",
        })
        .toFile(resizedPath);
    } catch (error) {
      console.log(error);
    }

    // const resizedUrl = `${DOMAIN}\\image\\${folderName}\\${shortDate}\\${closestWidth}\\${fileName}`;

    return resizedPath;
  }

  /**
   * @param {string} shortDate
   * @param {string} fileName
   * @param {string} folderName
   * @param {Array<number>} size Size which you want to crop, [width, height]
   */
  async crop(shortDate, fileName, size, folderName) {
    if (
      $.isEmpty(size) === true ||
      $.isEmpty(fileName) === true ||
      $.isEmpty(shortDate) === true ||
      $.isEmpty(folderName) === true
    ) {
      return null;
    }

    const originalPath = `${WORKING_DIR}\\uploads\\${folderName}\\${shortDate}\\${fileName}`;

    /**
     * @type {Array<number>}
     */
    let exactlySize = [];

    const definedSize = MEDIA_CONFIG[folderName];

    if ($.isEmpty(definedSize) === true) {
      throw new Error("invalid folder");
    }

    exactlySize = definedSize.crop.find(
      (x) => x[0] === size[0] && x[1] === size[1]
    );

    if ($.isEmpty(exactlySize) === true) {
      return this.resize(shortDate, fileName, size, folderName);
    }

    const uploadPath = `${WORKING_DIR}\\uploads\\${folderName}\\${shortDate}\\${exactlySize[0]}x${exactlySize[1]}`;
    const resizedPath = `${uploadPath}\\${fileName}`;

    if (fs.existsSync(uploadPath) === false) {
      fs.mkdirSync(uploadPath);
    }

    if (fs.existsSync(resizedPath) === true) {
      return resizedPath;
    }

    try {
      const dimensions = sizeOf(originalPath);

      let bIsWidthLargerThanHeight = false;

      if (dimensions.width > dimensions.height) {
        bIsWidthLargerThanHeight = true;
      }

      let resize = [];
      let cropConfig = {};

      if (bIsWidthLargerThanHeight === true) {
        // width > height
        resize = [null, exactlySize[1]];

        cropConfig = {
          left: POSITION_TO_START_CROP.left * exactlySize[0],
          top: 0,
          width: POSITION_TO_START_CROP.size * exactlySize[0],
          height: POSITION_TO_START_CROP.size * exactlySize[1],
        };
      } else {
        // width < height
        resize = [exactlySize[0], null];

        cropConfig = {
          left: 0,
          top: POSITION_TO_START_CROP.top * exactlySize[1],
          width: POSITION_TO_START_CROP.size * exactlySize[0],
          height: POSITION_TO_START_CROP.size * exactlySize[1],
        };
      }

      const resized = await sharp(originalPath)
        .resize(resize[0], resize[1])
        .extract(cropConfig)
        .toFile(resizedPath);
    } catch (error) {
      console.log(error);
    }

    // const resizedUrl = `${DOMAIN}\\image\\${folderName}\\${shortDate}\\${exactlySize[0]}x${exactlySize[1]}\\${fileName}`;

    return resizedPath;
  }

  async getImageUrl(shortDate, fileName, folderName, width = 0, height = 0) {
    const bIsResize = isResize([width, height]);

    if (bIsResize === true) {
      return `${DOMAIN}/image/${folderName}/${shortDate}/${width}}/${fileName}`;
    }

    return `${DOMAIN}/image/${folderName}/${shortDate}/${width}x${height}/${fileName}`;
  }
}

/**
 * @param {Array<number>} size
 * @returns {boolean}
 */
function isResize(size) {
  if (!size || size.length < 1) {
    return false;
  }

  if (size[0] === 0 || size[1] === 0) {
    return true;
  }

  return false;
}

module.exports = ImageBusiness;
