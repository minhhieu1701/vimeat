const $ = require("lodash");
const actionResultCode = require("../constant/action-result-code");
const { Hash, HASH_TYPE } = require("../module/util/hash");
const JWT = require("../module/jwt");
const rpUser = require("../repositories/user-repository");
const UserExtension = require("../module/mysql/extension/user-extension");

class AuthBusiness {
  constructor() {
    this._rpUser = new rpUser();
  }

  /**
   *
   * @param {LoginRequest} request
   * @returns  {Promise<LoginResponse>} result
   */
  async Login(request) {
    /**@type {LoginResponse} */
    const result = {
      resultCode: actionResultCode.FAIL,
      authentication: {},
    };

    if ($.isEmpty(request) === true) {
      return result;
    } else if ($.isEmpty(request.username) === true) {
      if ($.isEmpty(request.email) === true) {
        return result;
      } else if (REGEX.EMAIL.test(request.email) === false) {
        result.resultCode = actionResultCode.INVALID_EMAIL;
        return result;
      }
    }

    const enUser = await this._rpUser.getByUsername(request.username);
    if ($.isEmpty(enUser) === true) {
      result.resultCode = actionResultCode.FORBIDDEN;
      return result;
    }

    //check password
    const requestPassword = Hash.password(
      HASH_TYPE.MD5,
      request.password,
      enUser.salt
    );

    if ($.isEqual(enUser.password, requestPassword) === false) {
      result.resultCode = actionResultCode.FORBIDDEN;
      return result;
    }

    result.resultCode = actionResultCode.SUCCESS;
    result.authentication = {
      token: JWT.createToken(enUser),
      fullName: UserExtension.getDisplayName(
        enUser.first_name,
        enUser.last_name
      ),
      uid: enUser.uid,
    };

    return result;
  }
}

module.exports = new AuthBusiness();

/**
 * Declare our returned type
 *
 * @typedef {Object} LoginRequest
 * @property {string} username
 * @property {string} email
 * @property {string} password
 *
 * @typedef {Object} LoginResponse
 * @property {ResultCode} resultCode
 * @property {Authentication} authentication
 *
 * @typedef {Object} Authentication
 * @property {string} fullName
 * @property {string} uid
 * @property {string} token
 *
 * /**
 * @typedef {import("../constant/action-result-code").ResultCode} ResultCode
 */
