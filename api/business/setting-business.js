const { Partition, GetPageSize } = require("../module/util/paging");
const $ = require("lodash");
const rpSetting = require("../repositories/setting-repository");

class SettingBusiness {
  constructor() {
    this._rpSetting = new rpSetting();
  }

  /**
   * @param {InsertRequest} model setting model
   * @returns {Promise<boolean>} response
   */
  async insert(model) {
    if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.key) === true) {
      return false;
    } else if ($.isEmpty(model.value) === true) {
      return false;
    }

    /** @type {ResponseItem} */
    const entity = {
      key: model.key,
      value: model.value,
    };

    return await this._rpSetting.insert(entity);
  }

  /**
   * @param {number} id id
   * @param {string} value value
   * @returns {Promise<boolean>} response
   */
  async update(id, value) {
    if (!id || id < 1) {
      return false;
    } else if ($.isEmpty(value) === true) {
      return false;
    }

    /**
     * @type {ResponseItem}
     */
    const entity = {
      id: id,
      value: value,
    };

    return await this._rpSetting.update(entity);
  }

  /**
   * @param {number} id uid
   * @returns {Promise<boolean>} response
   */
  async delete(id) {
    if (!id || id < 1) {
      return false;
    }

    return await this._rpSetting.delete(id);
  }

  /**
   * @param {number} id id
   * @returns {Promise<ResponseItem>} response
   */
  async get(id) {
    if (!id || id < 1) {
      return false;
    }

    return await this._rpSetting.get(id, null);
  }

  /**
   * @param {number} requestCount request count
   * @param {number} requestPageNumber page number
   * @param {string} key
   * @returns {Promise<{
   *    totalPage: number
   *    records: any[]
   * }>} response
   */
  async getList(requestCount, requestPageNumber, key = null) {
    let lstId;
    let result = {
      totalPage: 0,
      totalItem: 0,
      records: [],
    };

    if ($.isEmpty(key) === false) {
      //search by key
      lstId = await this._rpSetting.getIdListByKey(key);
    } else {
      lstId = await this._rpSetting.getIdList();
    }

    if ($.isEmpty(lstId) === true) {
      return [];
    }

    const iTotalItem = lstId.length;
    const partitionUid = [];

    const iTotalPage = GetPageSize(requestCount, iTotalItem);

    const { startIndex, endIndex } = Partition(
      requestCount,
      requestPageNumber,
      iTotalItem
    );

    for (let index = startIndex; index < endIndex; index++) {
      partitionUid.push(lstId[index].id);
    }

    if (!partitionUid || partitionUid.length < 1) {
      return [];
    }

    const lstSetting = await this._rpSetting.getListByIds(
      partitionUid.join(","),
      null
    );

    if ($.isEmpty(lstSetting) === true) {
      return [];
    }

    result = {
      totalPage: iTotalPage,
      totalItem: iTotalItem,
      records: [],
    };

    lstSetting.forEach((setting) => {
      result.records.push({
        id: setting.id,
        key: setting.key,
        value: setting.value,
      });
    });

    return result;
  }
}

module.exports = new SettingBusiness();

/**
 * Declare our returned type
 * @typedef {import("../module/mysql/models/setting").Setting} ResponseItem
 * /**
 * @typedef {import("../constant/action-result-code").ResultCode} ResultCode
 *
 * @typedef {Object} InsertRequest
 * @property {string} key
 * @property {string} value
 *
 * @typedef {import("../models/shared/get-list").GetList} GetList
 *
 */
