const $ = require("lodash");
const rpNews = require("../repositories/news-repository");
const { Partition, GetPageSize } = require("../module/util/paging");
const Property = require("../module/util/property");
const { getNewsPhotoUrl } = require("../module/util/media");

class NewsBusiness {
  constructor() {
    this._rpNews = new rpNews();
  }

  sizes = [
    [0, 0],
    [600, 400],
    [1200, 800],
  ];

  /**
   * @param {InsertRequest} model news model
   * @param {number} userId current user id
   * @returns {Promise<boolean>} response
   */
  async insert(model, userId) {
    if ($.isEmpty(model) === true) {
      return false;
    } else if (!userId || userId < 1) {
      return false;
    } else if ($.isEmpty(model.title) === true) {
      return false;
    } else if ($.isEmpty(model.publishFrom) === true) {
      return false;
    }

    /**
     * @type {ResponseItem}
     */
    const entity = {
      description: model.description || null,
      photo: model.photo || null,
      publish_from: model.publishFrom,
      publish_to: model.publishTo || null,
      title: model.title,
      is_active: model.isActive,
      uid: new Date().getTime(),
      url_rewrite: model.urlRewrite,
      created_by: userId,
    };

    return await this._rpNews.insert(entity);
  }

  /**
   * @param {string} uid uid
   * @param {UpdateRequest} model news model
   * @returns {Promise<boolean>} response
   */
  async update(uid, model) {
    if ($.isEmpty(uid) === true) {
      return false;
    } else if ($.isEmpty(model) === true) {
      return false;
    } else if ($.isEmpty(model.title) === true) {
      return false;
    } else if ($.isEmpty(model.publishFrom) === true) {
      return false;
    }

    /**
     * @type {ResponseItem}
     */
    const entity = {
      description: model.description || null,
      is_active: model.isActive,
      photo: model.photo || null,
      publish_from: model.publishFrom,
      publish_to: model.publishTo || null,
      title: model.title,
      uid: uid,
    };

    return await this._rpNews.update(entity);
  }

  /**
   * @param {string} uid uid
   * @returns {Promise<boolean>} response
   */
  async delete(uid) {
    if ($.isEmpty(uid) === true) {
      return false;
    }

    return await this._rpNews.delete(uid);
  }

  /**
   * @param {string} uid uid
   * @returns {Promise<ResponseItem>} response
   */
  async getByUid(uid, isAdmin = false) {
    if ($.isEmpty(uid) === true) {
      return false;
    }

    const entity = await this._rpNews.getByUid(uid);

    if (entity) {
      delete entity.id;
    }

    if (isAdmin === false) {
      try {
        const bIsSuccess = await this._rpNews.increaseTotalView(uid);
        delete entity.is_active;
      } catch (error) {}
    }

    const model = {
      uid: entity.uid,
      title: entity.title,
      urlRewrite: entity.url_rewrite,
      description: entity.description,
      publishFrom: new Date(entity.publish_from).getTime(),
      publishTo: entity.publish_to
        ? new Date(entity.publish_to).getTime()
        : null,
      isActive: Property.bindBoolean(entity.is_active),
    };

    model.photo = getNewsPhotoUrl(entity.photo, this.sizes);

    return model;
  }

  /**
   * @param {number} requestCount request count
   * @param {string | null} lastUid last uid of previous request
   * @returns {Promise<GetList>} response
   */
  async getList(requestCount, lastUid = null) {
    const lstUid = await this._rpNews.getUidList(true);

    if ($.isEmpty(lstUid) === true) {
      return [];
    }

    let idx = 0;
    let iTotalRemain = 0;
    const partitionUid = [];

    if (lstUid.length > requestCount) {
      iTotalRemain = lstUid.length;
    }

    if ($.isEmpty(lastUid) === false) {
      lstUid.forEach((uid) => {
        idx++;

        iTotalRemain--;

        if ($.isEqual(uid, lastUid) === true) {
          return;
        }
      });
    }

    for (let i = 0, j = lstUid.length; idx < j; idx++) {
      if (i === requestCount) {
        break;
      }

      partitionUid.push(lstUid[idx].uid);

      i++;
    }

    if (partitionUid.length < 1) {
      return [];
    }

    const lstNews = await this._rpNews.getListByUids(
      partitionUid.join(","),
      true
    );

    if ($.isEmpty(lstNews) === true) {
      return [];
    }

    /**
     * @type {GetList}
     */
    const result = {
      totalRemain: iTotalRemain,
      records: [],
    };

    lstNews.forEach((news) => {
      const x = {
        uid: news.uid,
        title: news.title,
        url_rewrite: news.url_rewrite,
        description: news.description,
      };

      model.photo = getNewsPhotoUrl(news.photo, this.sizes);

      result.records.push(x);
    });

    return result;
  }

  /**
   * @param {number} requestCount request count
   * @param {number} requestPageNumber page number
   * @param {string} uid
   * @param {string} keyword
   * @param {boolean} isActive
   * @returns {Promise<{
   *    totalPage: number
   *    records: any[]
   * }>} response
   */
  async getAdminList(
    requestCount,
    requestPageNumber,
    uid = null,
    keyword = null,
    isActive = null
  ) {
    let lstUid;
    let result = {
      totalPage: 0,
      totalItem: 0,
      records: [],
    };

    if ($.isEmpty(uid) === false) {
      lstUid = [
        {
          uid,
        },
      ];
    } else if ($.isEmpty(keyword) === false) {
      //search by title
      lstUid = await this._rpNews.getUidListByKeyword(keyword, isActive);
    } else {
      lstUid = await this._rpNews.getUidList(isActive);
    }

    if ($.isEmpty(lstUid) === true) {
      return [];
    }

    const iTotalItem = lstUid.length;
    const partitionUid = [];

    const iTotalPage = GetPageSize(requestCount, iTotalItem);

    const { startIndex, endIndex } = Partition(
      requestCount,
      requestPageNumber,
      iTotalItem
    );

    for (let index = startIndex; index < endIndex; index++) {
      partitionUid.push(lstUid[index].uid);
    }

    if (partitionUid.length < 1) {
      return [];
    }

    const lstNews = await this._rpNews.getListByUids(
      partitionUid.join(","),
      null
    );

    if ($.isEmpty(lstNews) === true) {
      return [];
    }

    result = {
      totalPage: iTotalPage,
      totalItem: iTotalItem,
      records: [],
    };

    lstNews.forEach((news) => {
      result.records.push({
        uid: news.uid,
        title: news.title,
        urlRewrite: news.url_rewrite,
        description: news.description,
        publishFrom: new Date(news.publish_from).getTime(),
        publishTo: news.publish_to ? new Date(news.publish_to).getTime() : null,
        isActive: Property.bindBoolean(news.is_active),
        photo: news.photo,
      });
    });

    return result;
  }
}

module.exports = new NewsBusiness();

/**
 * Declare our returned type
 * @typedef {import("../module/mysql/models/news").News} ResponseItem
 * /**
 * @typedef {import("../constant/action-result-code").ResultCode} ResultCode
 *
 * @typedef {Object} InsertRequest
 * @property {string} title
 * @property {string} description
 * @property {DateTime} publishFrom
 * @property {DateTime} publishTo
 * @property {string} photo
 *
 *
 * @typedef {Object} UpdateRequest
 * @property {string} uid
 * @property {string} title
 * @property {string} description
 * @property {DateTime} publishFrom
 * @property {DateTime} publishTo
 * @property {boolean} isActive
 * @property {string} photo
 *
 * @typedef {import("../models/shared/get-list").GetList} GetList
 *
 */
