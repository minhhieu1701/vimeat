const expect = require("chai").expect;
const { getImageUrl } = require("../module/util/media");

describe("Generate url from photo", function () {
  describe("1 photo and 3 size", function () {
    it("converts the basic colors", function () {
      const result = getImageUrl("news", "3_2021/image_1616150472906.jpg", [
        [0, 0],
        [600, 400],
        [1200, 800],
      ]);

      expect(result.length).to.equal(3);
    });
  });
});
