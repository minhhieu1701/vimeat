"use strict";

require("./extension/array");
require("dotenv").config();

const Hapi = require("@hapi/hapi");
const MySwagger = require("./module/swagger");
const MySql = require("./module/mysql");
const JWT = require("./module/jwt");
const OverrideResponse = require("./module/override-response");

const init = async () => {
  const server = Hapi.server({
    port: process.env.PORT,
    host: process.env.HOST,
    routes: {
      cors: {
        origin: ["*"],
        credentials: true,
      },
    },
  });

  OverrideResponse.UnAuthorized(server);

  await JWT.init(server);

  await MySwagger.init(server);

  await MySql.start();

  await server.start((err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Server running at:", server.info.uri);
    }
  });

  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
