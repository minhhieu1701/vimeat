import MyLayout from "../../templates/layout";

export default function Index() {
  return (
    <MyLayout>
      <h1>This is home page ♫</h1>
    </MyLayout>
  );
}
