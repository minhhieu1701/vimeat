import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";

function MyLayout(props) {
  return (
    <div>
      <Head>
        <title>ViMeat</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>{props.children}</main>

      <footer>footer</footer>
    </div>
  );
}

MyLayout.propTypes = {
  children: PropTypes.element,
};

export default MyLayout;
