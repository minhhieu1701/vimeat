import React from "react";
import "./App.scss";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import { ContextProvider } from "./context/app-context";
import Home from "./pages/home";
import Login from "./pages/login";
import News from "./pages/news";
import Category from "./pages/category";
import NotFound from "./pages/404";
import Product from "./pages/product";
import Setting from "./pages/setting";

function App() {
  return (
    <ContextProvider>
      <Router>
        <div className="page">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/product" component={Product} />
            <Route path="/news" component={News} />
            <Route path="/category" component={Category} />
            <Route path="/setting" component={Setting} />
            <Route path="/login" component={Login} />
            <Route path="/404" component={NotFound} />
            <Redirect to="/404"></Redirect>
          </Switch>
        </div>
      </Router>
    </ContextProvider>
  );
}

export default App;
