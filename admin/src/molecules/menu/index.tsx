import * as React from "react";
import { Menu } from "antd";
import $ from "lodash";

export interface MenuItemProps {
  icon?: React.ReactNode;
  url: string;
  text: string;
}

interface IAppProps {
  className?: string;
  items: Array<MenuItemProps>;
}

export default function CustomMenu(props: IAppProps) {
  if ($.isEmpty(props.items) === true) {
    return null;
  }
  return (
    <Menu className={props.className}>
      {props.items.map((item, index) => {
        return (
          <Menu.Item key={index} icon={item.icon}>
            {item.text}
          </Menu.Item>
        );
      })}
    </Menu>
  );
}
