import {
  GoldFilled,
  FileOutlined,
  ShopOutlined,
  WindowsOutlined,
  ControlFilled,
} from "@ant-design/icons";
import "./index.scss";
import { Link } from "react-router-dom";
import { Menu } from "antd";
import React, { useEffect, useState } from "react";
import SubMenu from "antd/lib/menu/SubMenu";

interface IAppProps {}

const detectRoute = () => {
  const pathName = window.location.pathname;

  if (pathName.includes("news") === true) {
    return 2;
  } else if (pathName.includes("category") === true) {
    return 5;
  } else if (pathName.includes("product") === true) {
    return 6;
  } else if (pathName.includes("section") === true) {
    return 3;
  } else if (pathName.includes("setting") === true) {
    return 4;
  }

  return 1;
};

export default function NavigationList(props: IAppProps) {
  const [tabIndex, setTabIndex] = useState("1");

  useEffect(() => {
    const index = detectRoute();
    setTabIndex(index.toString());
  }, []);

  return (
    <Menu
      defaultSelectedKeys={["1"]}
      selectedKeys={[tabIndex]}
      mode="inline"
      theme="dark"
      inlineCollapsed={false}
    >
      <Menu.Item key="1" icon={<ShopOutlined />}>
        <Link to={"/"}>Trang chủ</Link>
      </Menu.Item>
      <Menu.Item key="2" icon={<FileOutlined />}>
        <Link to={"/news"}>Tin tức</Link>
      </Menu.Item>
      <SubMenu key="sub1" icon={<GoldFilled />} title="Sản phẩm" onTitleClick={() => {}}>
        <Menu.Item key="5">
          <Link to={"/category"}>Quản lý loại</Link>
        </Menu.Item>
        <Menu.Item key="6">
          <Link to={"/product"}>Quản lý sản phẩm</Link>
        </Menu.Item>
      </SubMenu>
      <Menu.Item key="3" icon={<WindowsOutlined />}>
        <Link to={"/section"}>Section</Link>
      </Menu.Item>
      <Menu.Item key="4" icon={<ControlFilled />}>
        <Link to={"/setting"}>Cài đặt chung</Link>
      </Menu.Item>
    </Menu>
  );
}
