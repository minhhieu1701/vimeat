import { Breadcrumb } from "antd";
import * as React from "react";

interface IBreadcrumItem {
  text: string;
  url?: string;
}

interface IAppProps {
  className?: string;
  items: Array<IBreadcrumItem>;
}

export default function Breadcrum(props: IAppProps) {
  return (
    <Breadcrumb className={props.className}>
      {props.items.map((item, index) => {
        return (
          <Breadcrumb.Item key={index}>
            {item.url ? (
              <a target="_blank" href={item.url}>
                {item.text}
              </a>
            ) : (
              item.text
            )}
          </Breadcrumb.Item>
        );
      })}
    </Breadcrumb>
  );
}
