import { ChangeEvent } from "react";
import { Input } from "antd";
import "./index.scss";

interface IAppProps {
  initColor?: string;
  onChangeComplete: (event: ChangeEvent<HTMLInputElement>) => void;
}

export default function ColorPicker(props: IAppProps) {
  return (
    <Input
      type="color"
      className="color-picker"
      value={props.initColor}
      defaultValue={props.initColor}
      onChange={props.onChangeComplete}
    />
  );
}
