import { Typography } from "antd";
import * as React from "react";

interface IAppProps {
  value: string;
  isDeleteStyle: boolean;
  style?: React.CSSProperties;
  className?: string;
}

export default function CurrencyText({
  className,
  isDeleteStyle,
  style,
  value,
}: IAppProps) {
  const formatted = `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return (
    <div style={style}>
      <Typography.Text
        style={style}
        className={className}
        delete={isDeleteStyle}
      >
        {formatted}
      </Typography.Text>
      đ
    </div>
  );
}
