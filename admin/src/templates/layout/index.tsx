import * as React from "react";
import * as antd from "antd";

import "./index.scss";
import { Context } from "../../context/app-context";
import { useContext, useEffect, useState } from "react";
import { vimeat } from "../../common/util";
import Config from "../../config";
import Cookie from "../../common/cookie";
import NavigationList from "../../molecules/navigation-list";
import UserDropdownMenu from "../../organisms/user-dropdown-menu";

const { Layout, Typography, Spin } = antd;
const { Header, Content, Footer, Sider } = Layout;

export interface IAppProps {
  children: React.ReactNode;
}

export default function App({ children }: IAppProps) {
  const [name, setName] = useState("");
  const state = useContext(Context);

  useEffect(() => {
    const nameCookie = Cookie.get(Config.cookie.user.name);

    if (nameCookie) {
      setName(nameCookie);
    }
  }, []);

  const bHadLoggedIn = vimeat.checkLogin();

  if (bHadLoggedIn === false) {
    window.location.href = "/login";

    return null;
  }

  return (
    <Layout>
      <Sider breakpoint="lg" collapsedWidth="0" className="layout__sider">
        <div className="logo">ViMeat</div>
        <NavigationList></NavigationList>
      </Sider>
      <Layout className="layout__main">
        <Header className="site-layout__sub-header-background">
          <Typography.Text strong style={{ paddingRight: "16px" }}>
            {name}
          </Typography.Text>
          <UserDropdownMenu></UserDropdownMenu>
        </Header>
        <Spin spinning={state?.isShowingPreloader}>
          <Content className="cover-margin">
            <div
              className="site-layout-background"
              style={{ padding: 24, minHeight: 360 }}
            >
              {children}
            </div>
          </Content>
        </Spin>
        <Footer style={{ textAlign: "center" }} className="site-layout__footer">
          ViMeat {new Date().getFullYear()} Developed by H
        </Footer>
      </Layout>
    </Layout>
  );
}
