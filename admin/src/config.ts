const API_URL = "http://localhost:3001";

const Config = {
  apiHost: API_URL,
  uploadUrl: `${API_URL}/upload/image`,
  cookie: {
    tokenKey: "vimeat.token",
    user: {
      name: "vimeat.user.name",
    },
  },
  notification:{
    duration: 3 //second
  },
  //reference: https://www.tiny.cloud/my-account/dashboard
  tinyMce: {
    token: "m8f5refhleqve8iwl6muaqxgcessj9icmcw9vj2rkhjltsj7",
    folder: "",
    config: {
      height: 500,
      menubar: false,
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste code help wordcount",
      ],
      toolbar: [
        "undo redo | formatselect | bold italic backcolor",
        "alignleft aligncenter alignright alignjustify | image",
        "bullist numlist outdent indent | removeformat | help",
      ],
      images_file_types: "jpg, jpeg, png",
      automatic_uploads: true,
      images_upload_url: `${API_URL}/upload/image`,
      images_upload_credentials: true,
      images_upload_handler: async function (
        blobInfo: any,
        success: any,
        failure: any
      ) {},
    },
  },
};

export default Config;
