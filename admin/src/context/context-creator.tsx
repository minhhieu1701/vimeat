import produce, { Draft } from "immer";
import * as React from "react";
import { Dispatch } from "react";

/**
 * Define action interface
 */
interface Action {
  type: string;
  payload?: any;
}

/**
 * Define dispatch interface
 */
interface ContextDispatch<Y> {
  dispatch: Dispatch<Y>;
}

/**
 * Define reducer interface base on draft of immer
 */
interface DraftReducer<X, Y> {
  (this: Draft<X>, draft: Draft<X>, action: Y): X;
}

function contextCreator<X extends object, Y extends Action>(
  initialState: X,
  reducer: DraftReducer<X, Y>
) {
  const Context = React.createContext<(X & ContextDispatch<Y>) | null>(null);

  const ContextProvider = ({ children }: any) => {
    const immerReducer = produce(reducer) as React.Reducer<X, Y>;

    const [state, dispatch] = React.useReducer(immerReducer, initialState);

    return (
      <Context.Provider value={{ ...state, dispatch: dispatch }}>
        {children}
      </Context.Provider>
    );
  };

  return { Context, ContextProvider };
}

export default contextCreator;
