/**
 * Create context
 */

import { Draft } from "immer";
import contextCreator from "./context-creator";

/**
 * Define interface for state
 */
export interface IState {
  isShowingPreloader: boolean;
}

/**
 * Define interface for action
 */
interface IAction {
  type: string;
  payload?: any;
}

/**
 * Define state base on interface IState
 */
const initialState: IState = {
  isShowingPreloader: false,
};

/**
 * Define enum action
 */
export enum Action {
  UPDATE_PRELOADER_STATUS = "UPDATE_PRELOADER_STATUS",
}

/**
 * Define reducer
 */
const reducer = (draft: Draft<IState>, action: IAction) => {
  switch (action.type) {
    case Action.UPDATE_PRELOADER_STATUS:
      draft.isShowingPreloader = action.payload;
      return draft;
    default:
      return draft;
  }
};

const { Context, ContextProvider } = contextCreator<IState, IAction>(
  initialState,
  reducer
);

export { Context, ContextProvider };
