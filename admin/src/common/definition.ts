export interface IResponse<T> {
  code: Number;
  message: String;
  data: T;
}

export interface IFormData {
  uid: string;
  title: string;
  description: string;
  publishFrom: moment.Moment;
  publishTo?: moment.Moment | null;
  isActive: boolean;
  photo: string;
  urlRewrite: string;
}

export interface INews {
  uid: string;
  title: string;
  description: string;
  publishFrom: moment.Moment;
  publishTo?: moment.Moment | null;
  isActive: boolean;
  photo: Array<IPhotoItem>;
  urlRewrite: string;
}

export interface IPhotoItem {
  name: string;
  url: string;
  height: number;
  width: number;
}

export interface INotification {
  title?: string | null;
  content: string;
}
