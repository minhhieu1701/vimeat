export default class StringBuffer {
  buffer: Array<string>;
  index: number;
  constructor() {
    this.buffer = [];
    this.index = 0;
  }

  append(s: string) {
    this.buffer[this.index] = s;
    this.index += 1;

    return this;
  }

  toString() {
    return this.buffer.join("");
  }
}
