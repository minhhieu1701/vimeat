import { notification } from "antd";
import { INotification } from "./definition";
import { CheckCircleFilled } from "@ant-design/icons";
import Config from "../config";

export const Notification = {
  info: (args: INotification) => {
    notification.open({
      message: args.title || "Thông báo",
      duration: Config.notification.duration,
      description: args.content,
      icon: <CheckCircleFilled />,
      className: "vimeat-notification-info",
    });
  },
  success: (args: INotification) => {
    notification.open({
      message: args.title || "Thông báo",
      duration: Config.notification.duration,
      description: args.content,
      icon: <CheckCircleFilled />,
      className: "vimeat-notification-success",
    });
  },
  error: (args: INotification) => {
    notification.open({
      message: args.title || "Thông báo",
      duration: Config.notification.duration,
      description: args.content,
      icon: <CheckCircleFilled />,
      className: "vimeat-notification-error",
    });
  },
  warning: (args: INotification) => {
    notification.open({
      message: args.title || "Thông báo",
      duration: Config.notification.duration,
      description: args.content,
      icon: <CheckCircleFilled />,
      className: "vimeat-notification-warning",
    });
  },
};
