import $ from "lodash";
import axios, { AxiosRequestConfig } from "axios";
import cookie from "./cookie";
import Popup from "./popup";
import appConfig from "../config";
import { ResultCode } from "../common/constant";

const DefaultOption: AxiosRequestConfig = {
  method: "get",
  baseURL: `${process.env.HOST}${
    process.env.PORT ? ":" + process.env.PORT : ""
  }/`,
  headers: {
    Authorization: cookie.get(appConfig.cookie.tokenKey),
    "Content-Type": "application/json",
  },
};

export const defaultOption = DefaultOption;

export const cloneObject = (object: any) => {
  return JSON.parse(JSON.stringify(object));
};

export const isAsyncFunction = (func: Function) => {
  if (!func || func.constructor.name !== "AsyncFunction") {
    return false;
  }

  return true;
};

export const formatMoney = (number: number) => {
  return `${number.toLocaleString("vn")}`;
};

export const parseMoney = (money: string) => {
  return money.replace(/\D/g, "");
};

export const vimeat = {
  checkLogin: (): Boolean => {
    const tokenCookie = cookie.get(appConfig.cookie.tokenKey);

    if ($.isEmpty(tokenCookie) === true) {
      const displayNameCookie = cookie.get(appConfig.cookie.user.name);

      if ($.isEmpty(displayNameCookie) === true) {
        cookie.set(appConfig.cookie.tokenKey, "", -1);

        return false;
      }

      return false;
    }

    return true;
  },
  /**
   * @param pathName the path of api
   * @param option The option per request
   * @return response
   */
  delete: async (
    pathName: string,
    data?: Object,
    option: AxiosRequestConfig = DefaultOption
  ) => {
    if ($.isEmpty(pathName) === true) {
      return null;
    }

    const resp = await axios.delete(`${appConfig.apiHost}${pathName}`, {
      headers: option.headers,
      data: data,
    });

    if (resp.status === 200) {
      if (resp.data.code && resp.data.code === ResultCode.UNAUTHORIZED.value) {
        Popup.error(
          "Thông báo",
          "Phiên đăng nhập hết hạn, vui lòng đăng nhập lại để tiếp tục!",
          () => {
            window.location.href = "/login";
          }
        );
      }
      return resp.data;
    } else {
      throw new Error(`${resp.status} - ${resp.statusText}`);
    }
  },
  /**
   * @param pathName the path of api
   * @param option The option per request
   * @return response
   */
  get: async (pathName: string, option: AxiosRequestConfig = DefaultOption) => {
    if ($.isEmpty(pathName) === true) {
      return null;
    }

    const resp = await axios.get(`${appConfig.apiHost}${pathName}`, option);

    if (resp.status === 200) {
      if (resp.data.code && resp.data.code === ResultCode.UNAUTHORIZED.value) {
        Popup.error(
          "Thông báo",
          "Phiên đăng nhập hết hạn, vui lòng đăng nhập lại để tiếp tục!",
          () => {
            window.location.href = "/login";
          }
        );
      }
      return resp.data;
    } else {
      throw new Error(`${resp.status} - ${resp.statusText}`);
    }
  },
  getBase64: (file: any) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  },
  /**
   * @param pathName the path of api
   * @param option The option per request
   * @return response
   */
  post: async (
    pathName: string,
    data?: any,
    option: AxiosRequestConfig = DefaultOption
  ) => {
    if ($.isEmpty(pathName) === true) {
      return null;
    }

    const resp = await axios.post(
      `${appConfig.apiHost}${pathName}`,
      data,
      option
    );

    if (resp.status === 200) {
      if (resp.data.code && resp.data.code === ResultCode.UNAUTHORIZED.value) {
        Popup.error(
          "Thông báo",
          "Phiên đăng nhập hết hạn, vui lòng đăng nhập lại để tiếp tục!",
          () => {
            window.location.href = "/login";
          }
        );
      }
      return resp.data;
    } else {
      throw new Error(`${resp.status} - ${resp.statusText}`);
    }
  },
  /**
   * @param pathName the path of api
   * @param option The option per request
   * @return response
   */
  put: async (
    pathName: string,
    data: Object,
    option: AxiosRequestConfig = DefaultOption
  ) => {
    if ($.isEmpty(pathName) === true) {
      return null;
    }

    const resp = await axios.put(
      `${appConfig.apiHost}${pathName}`,
      data,
      option
    );

    if (resp.status === 200) {
      if (resp.data.code && resp.data.code === ResultCode.UNAUTHORIZED.value) {
        Popup.error(
          "Thông báo",
          "Phiên đăng nhập hết hạn, vui lòng đăng nhập lại để tiếp tục!",
          () => {
            window.location.href = "/login";
          }
        );
      }
      return resp.data;
    } else {
      throw new Error(`${resp.status} - ${resp.statusText}`);
    }
  },
};
