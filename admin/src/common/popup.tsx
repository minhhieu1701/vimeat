import {
  CheckCircleFilled,
  ExclamationCircleFilled,
  QuestionCircleFilled,
} from "@ant-design/icons";
import { Modal } from "antd";
import React from "react";
import $ from "lodash";
import { isAsyncFunction } from "./util";

const checkedIcon: React.ReactNode = <CheckCircleFilled />;
const errorIcon: React.ReactNode = <ExclamationCircleFilled />;
const confirmIcon: React.ReactNode = <QuestionCircleFilled />;

const Popup = {
  success: (
    title: string,
    content: string,
    onClickOk?: Function,
    icon: React.ReactNode = checkedIcon
  ) => {
    Modal.success({
      title: title,
      icon: icon,
      content: content,
      className: "vimeat-popup-success",
      okText: "Xác nhận",
      onOk: (args: any) => {
        if (onClickOk && $.isFunction(onClickOk) === true) {
          onClickOk(args);
        }
      },
    });
  },
  error: (
    title: string,
    content: string,
    onClickOk?: Function,
    icon: React.ReactNode = errorIcon
  ) => {
    Modal.error({
      title: title,
      content: content,
      icon: icon,
      className: "vimeat-popup-error",
      okText: "Xác nhận",
      onOk: (args: any) => {
        if (onClickOk && $.isFunction(onClickOk) === true) {
          onClickOk(args);
        }
      },
    });
  },
  confirm: (
    title: string,
    content: string,
    onClickOk?: Function,
    icon: React.ReactNode = confirmIcon
  ) => {
    Modal.confirm({
      title: title,
      icon: icon,
      content: content,
      okText: "Xác nhận",
      cancelText: "Hủy",
      onOk: async (args: any) => {
        if (onClickOk && $.isFunction(onClickOk) === true) {
          if (isAsyncFunction(onClickOk) === true) {
            await onClickOk(args);
          } else {
            onClickOk(args);
          }
        }
      },
    });
  },
};

export default Popup;
