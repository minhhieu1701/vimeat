const MESSAGE = {
    "ERROR": "Có lỗi xảy ra, vui lòng thử lại!",
    "CONFIRM": "Bạn có chắc muốn xóa dòng này ?"
}

export default MESSAGE;