const cookie = {
  get: (key: string): string => {
    let name = key + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  },
  set: (key: string, value: string, expiredDay: number) => {
    let d = new Date();

    d.setTime(d.getTime() + expiredDay * 24 * 60 * 60 * 1000);

    var expires = "expires=" + d.toUTCString();

    document.cookie = key + "=" + value + ";" + expires + ";path=/";
  },
};

export default cookie;
