import { Editor } from "@tinymce/tinymce-react";
import { Form, Tabs, Input, DatePicker, Switch, Modal, Upload } from "antd";
import { IFormData, INews, IPhotoItem } from "../../common/definition";
import { PlusOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { cloneObject, vimeat as VIMEAT } from "../../common/util";
import Config from "../../config";
import MEDIA_FOLDER from "../../media-folder";
import moment from "moment";
import MyModal from "../../organisms/modal";
import NewsBusiness from "../../business/news-business";
import produce from "immer";
import UploadBusiness from "../../business/upload-business";
import { Notification } from "../../common/notification";

const _bizUpload = new UploadBusiness();
const _bizNews = new NewsBusiness();

interface IProps {
  data?: INews;
  isEdit: boolean;
  onAfterCreateSuccess: Function;
  onCancel: Function;
  visible: boolean;
}

interface IState {
  fileList: any;
  isDisabled: boolean;
  previewImage: string;
  previewTitle: string;
  previewVisible: boolean;
  tabIndex: string;
}

const initialData = {
  description: "",
  isActive: false,
  photo: "",
  publishFrom: moment(),
  title: "",
  publishTo: null,
  urlRewrite: "",
  uid: "",
};

export default function NewsForm({
  data,
  isEdit,
  onAfterCreateSuccess,
  onCancel,
  visible,
}: IProps) {
  const [form] = Form.useForm();
  const [content, setContent] = useState("");
  const [photo, setPhoto] = useState("");
  const [state, setState] = useState<IState>({
    previewImage: "",
    previewVisible: false,
    previewTitle: "",
    fileList: [],
    isDisabled: false,
    tabIndex: "1",
  });

  const customRequest = async ({ onSuccess, onError, file }: any) => {
    await _bizUpload.Upload(
      MEDIA_FOLDER.NEWS,
      file,
      (resp: any) => {
        onSuccess(null, resp);
        setPhoto(resp.fileName);
      },
      (message: string) => {
        onError(null, message);
      }
    );
  };

  const handleCancel = () => {
    setState(
      produce((draft) => {
        draft.previewVisible = false;
      })
    );
  };

  const handleCancelMyModal = () => {
    handleClearForm();
    onCancel();
  };

  const handleClearForm = () => {
    setState(
      produce((draft) => {
        draft.fileList = [];
        draft.tabIndex = "1";
      })
    );

    setContent("");

    form.resetFields();
  };

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await VIMEAT.getBase64(file.originFileObj);
    }

    setState(
      produce((draft) => {
        draft.previewImage = file.url || file.preview;
        draft.previewVisible = true;
        draft.previewTitle =
          file.name || file.url.substring(file.url.lastIndexOf("/") + 1);
      })
    );
  };

  const handleChange = ({ fileList }: any) => {
    setState(
      produce((draft) => {
        draft.fileList = fileList;
      })
    );
  };

  const handleChangeTab = (activeKey: string) => {
    setState(
      produce((draft) => {
        draft.tabIndex = activeKey;
      })
    );
  };

  const handleDisabledForm = (isDisabled: boolean) => {
    setState(
      produce((draft) => {
        draft.isDisabled = isDisabled;
      })
    );
  };

  const handleSubmit = async (formData: IFormData) => {
    handleDisabledForm(true);
    try {
      if (isEdit === false) {
        const bIsSuccess = await _bizNews.Create(formData);

        if (bIsSuccess === true) {
          Notification.success({
            content: "Tạo mới thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Tạo mới thất bại",
          });
        }
      } else {
        let bIsSuccess = false;
        if (data && data.uid) {
          formData.uid = data.uid;

          bIsSuccess = await _bizNews.Update(formData);
        }

        if (bIsSuccess === true) {
          Notification.success({
            content: "Cập nhật thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Cập nhật thất bại",
          });
        }
      }
    } finally {
      handleDisabledForm(false);
    }
  };

  const onEditorChange = (content: any, editor: any) => {
    setContent(content);
  };

  const onClickOk = async () => {
    const request: IFormData = cloneObject(initialData);

    form
      .validateFields()
      .then(async (values: any) => {
        request.title = values.title;
        request.publishFrom = values.publishFrom;
        request.publishTo = values.publishTo;
        request.isActive = values.isActive;
        request.description = content;
        request.photo = photo;
        request.urlRewrite = values.urlRewrite;
        await handleSubmit(request);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  

  useEffect(() => {
    if (isEdit === true && data) {
      form.setFieldsValue(data);
      if (data.photo) {
        setState({
          ...state,
          fileList: [
            {
              uid: data.uid,
              name: data.photo[0].name,
              url: data.photo[0].url,
              status: "done",
            },
          ],
        });

        setPhoto(data.photo[0].name);

        console.log(photo);
      }

      if (data.description) {
        setContent(data.description);
      }
    }
  }, [data]);

  const tinyMceConfig = { ...Config.tinyMce.config };
  tinyMceConfig.images_upload_handler = async (
    blobInfo: any,
    success: any,
    failure: any
  ) => {
    await _bizUpload.UploadForTinyMce(
      blobInfo,
      success,
      failure,
      MEDIA_FOLDER.NEWS
    );
  };

  return (
    <MyModal
      visible={visible}
      title="Create news"
      okButtonProps={{ disabled: state.isDisabled }}
      cancelButtonProps={{ disabled: state.isDisabled }}
      onOk={onClickOk}
      onCancel={() => handleCancelMyModal()}
    >
      <Form
        form={form}
        name="form"
        initialValues={initialData}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        onValuesChange={() => {}}
      >
        <Tabs
          activeKey={state.tabIndex}
          defaultActiveKey={"1"}
          onChange={(activeKey) => handleChangeTab(activeKey)}
        >
          <Tabs.TabPane tab="Basic info" key="1">
            <Form.Item
              label="Title"
              name="title"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tiêu đề",
                },
              ]}
            >
              <Input disabled={state.isDisabled} />
            </Form.Item>
            <Form.Item
              label="Url rewrite"
              name="urlRewrite"
              rules={[
                {
                  required: true,
                  message:
                    "Vui lòng nhập url hiển thị (ví dụ tiêu đề là Thịt heo giá sỉ, nhập là thit-heo-gia-si)",
                },
              ]}
            >
              <Input disabled={state.isDisabled} />
            </Form.Item>
            <Form.Item
              label="Publish from"
              name="publishFrom"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn thời gian",
                },
              ]}
            >
              <DatePicker
                disabled={state.isDisabled}
                showTime={{ format: "HH:mm" }}
                format="YYYY-MM-DD HH:mm"
              />
            </Form.Item>
            <Form.Item label="Publish to" name="publishTo">
              <DatePicker
                disabled={state.isDisabled}
                showTime={{ format: "HH:mm" }}
                format="YYYY-MM-DD HH:mm"
              />
            </Form.Item>
            <Form.Item label="Active" name="isActive" valuePropName="checked">
              <Switch />
            </Form.Item>
            <Form.Item label="Photo">
              <Upload
                disabled={state.isDisabled}
                action={Config.uploadUrl}
                customRequest={customRequest}
                listType="picture-card"
                fileList={state.fileList}
                onPreview={handlePreview}
                onChange={handleChange}
              >
                {state.fileList.length >= 1 ? null : uploadButton}
              </Upload>
            </Form.Item>
            <Modal
              visible={state.previewVisible}
              title={state.previewTitle}
              footer={null}
              onCancel={handleCancel}
            >
              <img
                alt="example"
                style={{ width: "100%" }}
                src={state.previewImage}
              />
            </Modal>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Description" key="2">
            <Editor
              disabled={state.isDisabled}
              apiKey={Config.tinyMce.token}
              initialValue={""}
              init={tinyMceConfig}
              value={content}
              onEditorChange={onEditorChange}
            />
          </Tabs.TabPane>
        </Tabs>
      </Form>
    </MyModal>
  );
}

const uploadButton = (
  <div>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>Upload</div>
  </div>
);
