import { cloneObject, vimeat as VIMEAT } from "../../common/util";
import { Editor } from "@tinymce/tinymce-react";
import {
  Form,
  Tabs,
  Input,
  DatePicker,
  Switch,
  Modal,
  Upload,
  Typography,
  Tooltip,
  InputNumber,
  Select,
} from "antd";
import { IProduct } from "../../business/interfaces/product/model";
import { Notification } from "../../common/notification";
import { PlusOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import Config from "../../config";
import MEDIA_FOLDER from "../../media-folder";
import MyModal from "../modal";
import produce from "immer";
import CategoryBusiness from "../../business/category-business";
import ProductBusiness from "../../business/product-business";
import UploadBusiness from "../../business/upload-business";
import Unit from "../../common/enum/shared/unit";
import IUpsertRequest from "../../business/interfaces/product/upsert-request";
import IEnumItem from "../../business/interfaces/shared/enum-item";
import $ from "lodash";

import "./index.scss";
import moment from "moment";

const _bizUpload = new UploadBusiness();
const _bizProduct = new ProductBusiness();
const _bizCategory = new CategoryBusiness();
const MAX_PHOTO = 6;

interface IProps {
  data?: IProduct;
  isEdit: boolean;
  onAfterCreateSuccess: Function;
  onCancel: Function;
  visible: boolean;
}

interface IState {
  description: string;
  fileList: any;
  isDisabled: boolean;
  photo: string[];
  previewImage: string;
  previewTitle: string;
  previewVisible: boolean;
  salePrice?: number;
  tabIndex: string;
  unitPrice: number;
  categories: Array<IEnumItem<number>>;
}

const initialData = {
  description: "",
  havePromotion: false,
  isActive: false,
  name: "",
  photo: null,
  publishFrom: "",
  publishTo: "",
  salePrice: null,
  shortDescription: "",
  unit: Unit.Kilograms.value,
  unitPrice: 0,
  weight: "",
};

const UNIT = Object.values(Unit);

export default function ProductForm({
  data,
  isEdit,
  onAfterCreateSuccess,
  onCancel,
  visible,
}: IProps) {
  const [form] = Form.useForm();
  const [state, setState] = useState<IState>({
    fileList: [],
    isDisabled: false,
    previewImage: "",
    previewTitle: "",
    previewVisible: false,
    tabIndex: "1",
    description: "",
    photo: [],
    unitPrice: 0,
    salePrice: undefined,
    categories: Array<IEnumItem<number>>(),
  });

  const customRequest = async ({ onSuccess, onError, file }: any) => {
    await _bizUpload.Upload(
      MEDIA_FOLDER.PRODUCT,
      file,
      (resp: any) => {
        onSuccess(null, resp);

        setState(
          produce((draft) => {
            const tmp = cloneObject(draft.photo);
            tmp.push(resp.fileName);
            draft.photo = tmp;
          })
        );

        // setPhoto(resp.fileName);
      },
      (message: string) => {
        onError(null, message);
      }
    );
  };

  const getCategoryEnumList = async () => {
    const result = await _bizCategory.GetEnumList();
    if (result) {
      setState({
        ...state,
        categories: result,
      });
    }
  };

  const handleCancel = () => {
    setState(
      produce((draft) => {
        draft.previewVisible = false;
      })
    );
  };

  const handleCancelMyModal = () => {
    handleClearForm();
    onCancel();
  };

  const handleClearForm = () => {
    setState(
      produce((draft) => {
        draft.fileList = [];
        draft.photo = [];
        draft.tabIndex = "1";
      })
    );

    setState(
      produce((draft) => {
        draft.description = "";
      })
    );

    form.resetFields();
  };

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await VIMEAT.getBase64(file.originFileObj);
    }

    setState(
      produce((draft) => {
        draft.previewImage = file.url || file.preview;
        draft.previewVisible = true;
        draft.previewTitle =
          file.name || file.url.substring(file.url.lastIndexOf("/") + 1);
      })
    );
  };

  const handleChange = ({ fileList }: any) => {
    setState(
      produce((draft) => {
        draft.fileList = fileList;
      })
    );
  };

  const handleChangeTab = (activeKey: string) => {
    setState(
      produce((draft) => {
        draft.tabIndex = activeKey;
      })
    );
  };

  const handleDisabledForm = (isDisabled: boolean) => {
    setState(
      produce((draft) => {
        draft.isDisabled = isDisabled;
      })
    );
  };

  const handleSubmit = async (formData: IUpsertRequest) => {
    handleDisabledForm(true);
    try {
      if (isEdit === false) {
        const bIsSuccess = await _bizProduct.Create(formData);

        if (bIsSuccess === true) {
          Notification.success({
            content: "Tạo mới thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Tạo mới thất bại",
          });
        }
      } else {
        let bIsSuccess = false;
        if (data && data.id) {
          bIsSuccess = await _bizProduct.Update(data.id, formData);
        }

        if (bIsSuccess === true) {
          Notification.success({
            content: "Cập nhật thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Cập nhật thất bại",
          });
        }
      }
    } finally {
      handleDisabledForm(false);
    }
  };

  const onEditorChange = (content: any, editor: any) => {
    setState(
      produce((draft) => {
        draft.description = content;
      })
    );
  };

  const onClickOk = async () => {
    const request: IUpsertRequest = cloneObject(initialData);

    form
      .validateFields()
      .then(async (values: any) => {
        request.name = values.name;
        request.isActive = values.isActive.toString();
        request.description = state.description;
        request.photo = state.photo;
        request.salePrice = values.salePrice;
        request.shortDescription = values.shortDescription;
        request.unit = +values.unit;
        request.unitPrice = values.unitPrice;
        request.weight = values.weight;
        request.urlRewrite = values.urlRewrite;
        request.categories = values.categories;

        if (values.publishFrom) {
          request.publishFrom = moment(values.publishFrom).format(
            "YYYY-MM-DD HH:mm"
          );
        }

        if (values.publishTo) {
          request.publishTo = moment(values.publishTo).format(
            "YYYY-MM-DD HH:mm"
          );
        }

        if (request.salePrice) {
          request.havePromotion = true;
        }

        await handleSubmit(request);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getCategoryEnumList();
  }, []);

  useEffect(() => {
    if (isEdit === true && data) {
      const cloneData = $.cloneDeepWith<any>(data);

      cloneData.unit = cloneData.unit.toString();

      form.setFieldsValue(cloneData);

      const tmp1 = new Array<any>();
      const tmp2 = new Array<string>();

      if (data.photos) {
        data.photos.forEach((photo) => {
          tmp1.push({
            uid: data.id,
            name: photo[0].name,
            url: photo[0].url,
            status: "done",
          });

          tmp2.push(photo[0].name);
        });
      }

      setState({
        ...state,
        fileList: tmp1,
        photo: tmp2,
        description: data.description,
      });
    }
  }, [data]);

  const tinyMceConfig = { ...Config.tinyMce.config };
  tinyMceConfig.images_upload_handler = async (
    blobInfo: any,
    success: any,
    failure: any
  ) => {
    await _bizUpload.UploadForTinyMce(
      blobInfo,
      success,
      failure,
      MEDIA_FOLDER.NEWS
    );
  };

  return (
    <MyModal
      visible={visible}
      title="Create product"
      okButtonProps={{ disabled: state.isDisabled }}
      cancelButtonProps={{ disabled: state.isDisabled }}
      onOk={onClickOk}
      onCancel={() => handleCancelMyModal()}
    >
      <Form
        form={form}
        name="form"
        initialValues={initialData}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        onValuesChange={() => {}}
      >
        <Tabs
          activeKey={state.tabIndex}
          defaultActiveKey={"1"}
          onChange={(activeKey) => handleChangeTab(activeKey)}
        >
          <Tabs.TabPane tab="Basic info" key="1">
            <Form.Item
              label="Name"
              name="name"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên sản phẩm",
                },
              ]}
            >
              <Input disabled={state.isDisabled} />
            </Form.Item>{" "}
            <Form.Item
              label="Url rewrite"
              name="urlRewrite"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên địa chỉ thay thế",
                },
              ]}
            >
              <Input disabled={state.isDisabled} />
            </Form.Item>
            <Form.Item
              label="Unit price"
              name="unitPrice"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập giá gốc",
                },
              ]}
            >
              <InputNumber
                style={{ width: "100%" }}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) =>
                  value ? value.replace(/\$\s?|(,*)/g, "") : ""
                }
              />
            </Form.Item>
            <Form.Item label="Sale price" name="salePrice">
              <InputNumber
                style={{ width: "100%" }}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) =>
                  value ? value.replace(/\$\s?|(,*)/g, "") : ""
                }
              />
            </Form.Item>
            <Form.Item label="Weight" required={true}>
              <Input.Group compact>
                <Form.Item name="weight" noStyle>
                  <InputNumber
                    style={{ width: "70%" }}
                    placeholder="Cân nặng"
                  />
                </Form.Item>
                <Form.Item
                  name="unit"
                  noStyle
                  rules={[{ required: true, message: "Vui lòng chọn đơn vị" }]}
                >
                  <Select style={{ width: "30%" }}>
                    {UNIT.map((item, index) => (
                      <Select.Option key={index} value={item.value}>
                        {item.text}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Input.Group>
            </Form.Item>
            <Form.Item
              name="categories"
              label="Category"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn ít nhất 1 loại cho sản phẩm",
                  type: "array",
                },
              ]}
            >
              <Select
                mode="multiple"
                className=""
                placeholder="Choose category"
              >
                {state.categories.map((category) => (
                  <Select.Option key={category.value} value={category.value}>
                    {category.text}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Publish from" name="publishFrom">
              <DatePicker
                disabled={state.isDisabled}
                showTime={{ format: "HH:mm" }}
                format="YYYY-MM-DD HH:mm"
              />
            </Form.Item>
            <Form.Item label="Publish to" name="publishTo">
              <DatePicker
                disabled={state.isDisabled}
                showTime={{ format: "HH:mm" }}
                format="YYYY-MM-DD HH:mm"
              />
            </Form.Item>
            <Form.Item label="Active" name="isActive" valuePropName="checked">
              <Switch />
            </Form.Item>
            <Form.Item label="Photo">
              <Tooltip title="Max 6 photos">
                <Upload
                  disabled={state.isDisabled}
                  action={Config.uploadUrl}
                  customRequest={customRequest}
                  listType="picture-card"
                  fileList={state.fileList}
                  onPreview={handlePreview}
                  onChange={handleChange}
                  maxCount={MAX_PHOTO}
                  multiple
                >
                  {state.fileList.length >= MAX_PHOTO ? null : uploadButton}
                </Upload>
              </Tooltip>
            </Form.Item>
            <Modal
              visible={state.previewVisible}
              title={state.previewTitle}
              footer={null}
              onCancel={handleCancel}
            >
              <img
                alt="example"
                style={{ width: "100%" }}
                src={state.previewImage}
              />
            </Modal>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Description" key="2">
            <Form.Item
              label="Short description"
              name="shortDescription"
              className="short-description"
            >
              <Input.TextArea
                showCount
                maxLength={150}
                style={{ marginBottom: "16px" }}
              />
            </Form.Item>
            <Typography.Text>Description:</Typography.Text>
            <Editor
              disabled={state.isDisabled}
              apiKey={Config.tinyMce.token}
              initialValue={""}
              init={tinyMceConfig}
              value={state.description}
              onEditorChange={onEditorChange}
            />
          </Tabs.TabPane>
        </Tabs>
      </Form>
    </MyModal>
  );
}

const uploadButton = (
  <div>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>Upload</div>
  </div>
);
