import * as React from "react";
import { Form, Input, Button, Typography } from "antd";
import AuthBusiness from "../../business/auth-business";
import Popup from "../../common/popup";
import Cookie from "../../common/cookie";
import Config from "../../config";

import "./index.scss";

interface IAppProps {}

export default function LoginPanel(props: IAppProps) {
  const _bizAuth = new AuthBusiness();

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 8,
    },
  };

  const [isProcessing, setProcessing] = React.useState(false);

  //Function
  //#region
  const onFinish = async (values: any) => {
    setProcessing(true);

    try {
      const resp = await _bizAuth.Login(values.username, values.password);

      if (!resp || resp.code !== 0) {
        Popup.error(
          "Thất bại",
          "Đăng nhập thất bại, vui lòng kiểm tra lại thông tin đăng nhập"
        );

        return;
      }

      Cookie.set(Config.cookie.tokenKey, resp.data.token, 1);
      Cookie.set(Config.cookie.user.name, resp.data.fullName, 1);

      setTimeout(() => {
        window.location.href = "/";
      }, 1000);
    } catch (error){
      console.log(error);
    } finally {
      setProcessing(false);
    }
  };
  //#endregion

  return (
    <div className="login-panel">
      <div className="login-panel__wrapper">
        <Typography.Title className="login-panel__title">
          Quản lý VIMEAT
        </Typography.Title>
        <Form {...layout} name="login" onFinish={onFinish}>
          <Form.Item
            label="Tên tài khoản"
            name="username"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên tài khoản!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Mật khẩu"
            name="password"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              loading={isProcessing}
              disabled={isProcessing}
            >
              Đăng nhập
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
