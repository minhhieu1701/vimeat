import { UserOutlined } from "@ant-design/icons";
import { Avatar, Dropdown } from "antd";
import * as React from "react";
import CustomMenu, { MenuItemProps } from "../../molecules/menu";

import "./index.scss";

interface IAppProps {}

const menuItems: Array<MenuItemProps> = [
  {
    text: "dong 1",
    url: "https://google.com",
  },
];

export default function UserDropdownMenu(props: IAppProps) {
  const itemDoms = (
    <CustomMenu className="user-dropdown-menu" items={menuItems}></CustomMenu>
  );

  return (
    <Dropdown overlay={itemDoms} trigger={["click"]}>
      <Avatar
        className="user-dropdown-menu__avatar"
        size={{
          xs: 24,
          sm: 32,
          md: 40,
          lg: 48,
          xl: 48,
          xxl: 48,
        }}
        icon={<UserOutlined />}
      >
      </Avatar>
    </Dropdown>
  );
}
