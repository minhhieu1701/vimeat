import { cloneObject, vimeat as VIMEAT } from "../../common/util";
import { Form, Input, Switch, Modal, Upload } from "antd";
import { ICategory } from "../../business/interfaces/news/model";
import { Notification } from "../../common/notification";
import { PlusOutlined } from "@ant-design/icons";
import { ChangeEvent, useEffect, useState } from "react";
import CategoryBusiness from "../../business/category-business";
import ColorPicker from "../../molecules/color-picker";
import Config from "../../config";
import ICreateRequest from "../../business/interfaces/category/create-request";
import MEDIA_FOLDER from "../../media-folder";
import MyModal from "../modal";
import produce from "immer";
import UploadBusiness from "../../business/upload-business";

const _bizUpload = new UploadBusiness();
const _bizCategory = new CategoryBusiness();

interface IProps {
  data?: ICategory;
  isEdit: boolean;
  onAfterCreateSuccess: Function;
  onCancel: Function;
  visible: boolean;
}

interface IState {
  background: string;
  borderColor: string;
  fileList: any;
  isDisabled: boolean;
  previewImage: string;
  previewTitle: string;
  previewVisible: boolean;
  sketchVisible1: boolean;
  sketchVisible2: boolean;
}

const initialData = {
  id: undefined,
  isActive: false,
  name: "",
  photo: "",
  background: "#ff7f50",
  borderColor: "#ff7f50",
};

export default function CategoryForm({
  data,
  isEdit,
  onAfterCreateSuccess,
  onCancel,
  visible,
}: IProps) {
  const [form] = Form.useForm();
  const [photo, setPhoto] = useState("");
  const [state, setState] = useState<IState>({
    previewImage: "",
    previewVisible: false,
    previewTitle: "",
    fileList: [],
    isDisabled: false,
    background: "#ff7f50",
    borderColor: "#ff7f50",
    sketchVisible1: false,
    sketchVisible2: false,
  });

  const customRequest = async ({ onSuccess, onError, file }: any) => {
    await _bizUpload.Upload(
      MEDIA_FOLDER.CATEGORY,
      file,
      (resp: any) => {
        onSuccess(null, resp);
        setPhoto(resp.fileName);
      },
      (message: string) => {
        onError(null, message);
      }
    );
  };

  const handleCancel = () => {
    setState(
      produce((draft) => {
        draft.previewVisible = false;
      })
    );
  };

  const handleCancelMyModal = () => {
    handleClearForm();
    onCancel();
  };

  const handleClearForm = () => {
    setState(
      produce((draft) => {
        draft.fileList = [];
        draft.background = "#ff7f50";
        draft.borderColor = "#ff7f50";
      })
    );

    form.resetFields();
  };

  const handlePreview = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await VIMEAT.getBase64(file.originFileObj);
    }

    setState(
      produce((draft) => {
        draft.previewImage = file.url || file.preview;
        draft.previewVisible = true;
        draft.previewTitle =
          file.name || file.url.substring(file.url.lastIndexOf("/") + 1);
      })
    );
  };

  const handleChange = ({ fileList }: any) => {
    setState(
      produce((draft) => {
        draft.fileList = fileList;
      })
    );
  };

  const handleChangeBackgroundColor = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;

    const fields = form.getFieldsValue();

    fields.background = value;

    setState({
      ...state,
      background: value,
    });

    form.setFieldsValue(fields);
  };

  const handleChangeBorderColor = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;

    const fields = form.getFieldsValue();

    fields.borderColor = value;

    setState({
      ...state,
      borderColor: value,
    });

    form.setFieldsValue(fields);
  };

  const handleDisabledForm = (isDisabled: boolean) => {
    setState(
      produce((draft) => {
        draft.isDisabled = isDisabled;
      })
    );
  };

  const handleSubmit = async (formData: ICreateRequest) => {
    handleDisabledForm(true);
    try {
      if (isEdit === false) {
        const bIsSuccess = await _bizCategory.Create(formData);

        if (bIsSuccess === true) {
          Notification.success({
            content: "Tạo mới thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Tạo mới thất bại",
          });
        }
      } else {
        let bIsSuccess = false;
        if (data && data.id) {
          bIsSuccess = await _bizCategory.Update(data.id, formData);
        }

        if (bIsSuccess === true) {
          Notification.success({
            content: "Cập nhật thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Cập nhật thất bại",
          });
        }
      }
    } finally {
      handleDisabledForm(false);
    }
  };

  const onClickOk = async () => {
    const request: ICreateRequest = cloneObject(initialData);

    form
      .validateFields()
      .then(async (values: any) => {
        request.name = values.name;
        request.isActive = values.isActive;
        request.photo = photo;
        request.background = values.background;
        request.borderColor = values.borderColor;
        await handleSubmit(request);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (isEdit === true && data) {
      form.setFieldsValue(data);
      if (data.photo) {
        setState({
          ...state,
          fileList: [
            {
              uid: data.id,
              name: data.photo[0].name,
              url: data.photo[0].url,
              status: "done",
            },
          ],
        });

        setPhoto(data.photo[0].name);
      }
      
      setState({
        ...state,
        background: data.background,
        borderColor: data.borderColor
      });
    }
  }, [data]);

  return (
    <MyModal
      visible={visible}
      title="Create category"
      okButtonProps={{ disabled: state.isDisabled }}
      cancelButtonProps={{ disabled: state.isDisabled }}
      onOk={onClickOk}
      onCancel={() => handleCancelMyModal()}
    >
      <Form
        form={form}
        name="form"
        initialValues={initialData}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        onValuesChange={() => {}}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên loại",
            },
          ]}
        >
          <Input disabled={state.isDisabled} />
        </Form.Item>
        <Form.Item name="background" label="Background color">
          <ColorPicker
            key="color-picker-1"
            initColor={state.background}
            onChangeComplete={(event) => {
              handleChangeBackgroundColor(event);
            }}
          />
        </Form.Item>
        <Form.Item name="borderColor" label="Border color">
          <ColorPicker
            key="color-picker-2"
            initColor={state.borderColor}
            onChangeComplete={(event) => {
              handleChangeBorderColor(event);
            }}
          />
        </Form.Item>
        <Form.Item label="Active" name="isActive" valuePropName="checked">
          <Switch />
        </Form.Item>
        <Form.Item label="Photo">
          <Upload
            disabled={state.isDisabled}
            action={Config.uploadUrl}
            customRequest={customRequest}
            listType="picture-card"
            fileList={state.fileList}
            onPreview={handlePreview}
            onChange={handleChange}
          >
            {state.fileList.length >= 1 ? null : uploadButton}
          </Upload>
        </Form.Item>
        <Modal
          visible={state.previewVisible}
          title={state.previewTitle}
          footer={null}
          onCancel={handleCancel}
        >
          <img
            alt="example"
            style={{ width: "100%" }}
            src={state.previewImage}
          />
        </Modal>
      </Form>
    </MyModal>
  );
}

const uploadButton = (
  <div>
    <PlusOutlined />
    <div style={{ marginTop: 8 }}>Upload</div>
  </div>
);
