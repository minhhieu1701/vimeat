import "./index.scss";
import { Modal, ModalProps } from "antd";

interface IAppProps extends ModalProps {
  children?: React.ReactNode | null;
}

export default function MyModal(props: IAppProps) {
  return (
    <Modal {...props} className="my-modal">
      {props.children}
    </Modal>
  );
}
