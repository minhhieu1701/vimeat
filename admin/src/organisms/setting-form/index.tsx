import { cloneObject } from "../../common/util";
import { Form, Input } from "antd";
import ISetting from "../../business/interfaces/setting/model";
import { Notification } from "../../common/notification";
import { useEffect, useState } from "react";
import SettingBusiness from "../../business/setting-business";
import ICreateRequest from "../../business/interfaces/setting/create-request";
import MyModal from "../modal";
import produce from "immer";

const _bizSetting = new SettingBusiness();

interface IProps {
  data?: ISetting;
  isEdit: boolean;
  onAfterCreateSuccess: Function;
  onCancel: Function;
  visible: boolean;
}

interface IState {
  isDisabled: boolean;
  key: string;
  value: string;
}

const initialData = {
  key: "",
  value: "",
};

export default function SettingForm({
  data,
  isEdit,
  onAfterCreateSuccess,
  onCancel,
  visible,
}: IProps) {
  const [form] = Form.useForm();
  const [state, setState] = useState<IState>({
    key: "",
    value: "",
    isDisabled: false,
  });

  const handleCancelMyModal = () => {
    handleClearForm();
    onCancel();
  };

  const handleClearForm = () => {
    setState(
      produce((draft) => {
        draft.key = "";
        draft.value = "";
      })
    );

    form.resetFields();
  };

  const handleDisabledForm = (isDisabled: boolean) => {
    setState(
      produce((draft) => {
        draft.isDisabled = isDisabled;
      })
    );
  };

  const handleSubmit = async (formData: ICreateRequest) => {
    handleDisabledForm(true);
    try {
      if (isEdit === false) {
        const bIsSuccess = await _bizSetting.Create(formData);

        if (bIsSuccess === true) {
          Notification.success({
            content: "Tạo mới thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Tạo mới thất bại",
          });
        }
      } else {
        let bIsSuccess = false;
        if (data && data.id) {
          bIsSuccess = await _bizSetting.Update(data.id, formData.value);
        }

        if (bIsSuccess === true) {
          Notification.success({
            content: "Cập nhật thành công",
          });
          handleClearForm();
          onAfterCreateSuccess();
        } else {
          Notification.error({
            content: "Cập nhật thất bại",
          });
        }
      }
    } finally {
      handleDisabledForm(false);
    }
  };

  const onClickOk = async () => {
    const request: ICreateRequest = cloneObject(initialData);

    form
      .validateFields()
      .then(async (values: any) => {
        request.key = values.key;
        request.value = values.value;
        await handleSubmit(request);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (isEdit === true && data) {
      form.setFieldsValue(data);
    }
  }, [data]);

  return (
    <MyModal
      visible={visible}
      title="Create setting"
      okButtonProps={{ disabled: state.isDisabled }}
      cancelButtonProps={{ disabled: state.isDisabled }}
      onOk={onClickOk}
      onCancel={() => handleCancelMyModal()}
    >
      <Form
        form={form}
        name="form"
        initialValues={initialData}
        layout="inline"
        onValuesChange={() => {}}
      >
        <Form.Item
          label="Key"
          name="key"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập từ khóa",
            },
          ]}
        >
          <Input disabled={state.isDisabled || isEdit === true} />
        </Form.Item>
        <Form.Item
          label="Value"
          name="value"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập giá trị",
            },
          ]}
        >
          <Input disabled={state.isDisabled} />
        </Form.Item>
      </Form>
    </MyModal>
  );
}
