import Layout from "../../templates/layout";

export interface IAppProps {}

export default function Home(props: IAppProps) {
  return <Layout>Home</Layout>;
}
