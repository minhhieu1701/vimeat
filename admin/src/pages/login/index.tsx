import * as antd from "antd";
import LoginPanel from '../../organisms/login-panel';

import "./index.scss";

const { Layout } = antd;
const { Content, Footer } = Layout;

interface IAppProps {}

export default function Login(props: IAppProps) {
  return (
    <Layout className="login">
      <Content className="login__content">
        <LoginPanel></LoginPanel>
      </Content>
      <Footer className="login__footer">ViMeat {new Date().getFullYear()}</Footer>
    </Layout>
  );
}
