import "./index.scss";
import {
  Button,
  Col,
  Input,
  Pagination,
  Row,
  Select,
  Space,
  Table,
  Tag,
  Tooltip,
} from "antd";
import {
  CheckOutlined,
  CloseOutlined,
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Action, Context } from "../../context/app-context";
import { ICategory } from "../../business/interfaces/news/model";
import { Notification } from "../../common/notification";
import { useEffect, useState, useContext } from "react";
import CategoryBusiness from "../../business/category-business";
import IGetListRequest from "../../business/interfaces/category/get-list-request";
import Layout from "../../templates/layout";
import MESSAGE from "../../common/message";
import CategoryForm from "../../organisms/category-form";
import Popup from "../../common/popup";
import produce from "immer";

interface IAppProps {}

interface ISearchRequest {
  id?: number;
  titleKeyword?: string;
  isActive: string;
  pageNumber: number;
  counter: number;
}

export default function NewsManagement(props: IAppProps) {
  const getColumns = () => {
    return [
      {
        title: "Id",
        dataIndex: "id",
        key: "id",
      },
      {
        title: "Name",
        dataIndex: "name",
        key: "name",
        ellipsis: {
          showTitle: false,
        },
        render: (text: string) => (
          <Tooltip placement="topLeft" title={text}>
            {text}
          </Tooltip>
        ),
      },
      {
        title: "Active",
        dataIndex: "isActive",
        key: "isActive",
        align: "center",
        render: (isActive: boolean) => {
          let color = isActive === true ? "green" : "red";
          let text = isActive === true ? <CheckOutlined /> : <CloseOutlined />;

          return (
            <span>
              <Tag className="my-tag" color={color} key={isActive.toString()}>
                {text}
              </Tag>
            </span>
          );
        },
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        align: "center",
        render: (text: string, record: any) => (
          <Space size="middle" className="my-space">
            <div className="ant-btn-group">
              <Button
                type="link"
                size={"small"}
                onClick={() => handleEditData(record.id)}
              >
                Edit
              </Button>
              <Button
                type="link"
                danger
                size={"small"}
                onClick={() => handleDelete(record.id, record.name)}
              > 
                Delete
              </Button>
            </div>
          </Space>
        ),
      },
    ];
  };
  const _bizCategory = new CategoryBusiness();
  const columns: any[] = getColumns();
  const [data, setData] = useState([]);
  const [detail, setDetail] = useState<ICategory | undefined>(undefined);
  const [isEdit, setEdit] = useState(false);
  const [totalItem, setTotalItem] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [searchRequest, setSearchRequest] = useState<ISearchRequest>({
    isActive: "true",
    pageNumber: 1,
    titleKeyword: "",
    id: undefined,
    counter: 0,
  });
  const [visible, setVisible] = useState(false);
  const state = useContext(Context);

  const getList = async (searchRequest: ISearchRequest) => {
    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: true,
    });

    try {
      const request: IGetListRequest = {
        requestCount: pageSize,
        page: searchRequest.pageNumber,
        id: searchRequest.id,
        keyword: searchRequest.titleKeyword,
      };

      if (searchRequest.isActive === "true") {
        request.isActive = true;
      } else if (searchRequest.isActive === "false") {
        request.isActive = false;
      }

      const result = await _bizCategory.GetList(request);
      if (result && result.data && result.data.records) {
        setData(result.data.records);
        setTotalItem(result.data.totalItem);
      } else {
        setData([]);
        setTotalItem(0);
      }
    } catch {
    } finally {
      state?.dispatch({
        type: Action.UPDATE_PRELOADER_STATUS,
        payload: false,
      });
    }
  };

  const handleClearFilter = async () => {
    setSearchRequest(
      produce((draft) => {
        draft.isActive = "true";
        draft.pageNumber = 1;
        draft.titleKeyword = "";
        draft.id = "";
        draft.counter++;
      })
    );
  };

  const handleIdBox = (event: React.FormEvent<HTMLInputElement>) => {
    setSearchRequest(
      produce((draft) => {
        draft.id = event.currentTarget.value;
      })
    );
  };

  const handleSelectBox = (value: string) => {
    setSearchRequest(
      produce((draft) => {
        draft.isActive = value;
      })
    );
  };

  const handleSearchButton = async () => {
    await getList(searchRequest);
  };

  const handleEditData = async (id: number) => {
    if (!id || id < 1) {
      Notification.error({
        title: "Lỗi",
        content: MESSAGE.ERROR,
      });

      return;
    }

    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: true,
    });

    setEdit(true);

    const detail = await _bizCategory.Get(id);

    if (!detail) {
      Notification.error({
        title: "Lỗi",
        content: MESSAGE.ERROR,
      });
    } else {
      setDetail(detail);
    }

    setVisible(true);

    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: false,
    });
  };

  const handleDelete = async (id: Number, name: string) => {
    Popup.confirm("Thông báo", `${MESSAGE.CONFIRM} ("${name}")`, async () => {
      const bIsSuccess = await _bizCategory.Delete(id);

      if (bIsSuccess === true) {
        Notification.success({
          content: "Xóa thành công",
        });

        setSearchRequest(
          produce((draft) => {
            draft.counter = draft.counter + 1;
          })
        );
      } else {
        Notification.error({
          content: "Xóa thất bại",
        });
      }
    });
  };

  const resetEditStatus = () => {
    setVisible(false);
    setEdit(false);
  };

  const handleAfterCreateSuccess = async () => {
    resetEditStatus();
    setSearchRequest(
      produce((draft) => {
        draft.counter = draft.counter + 1;
      })
    );
  };

  useEffect(() => {
    getList(searchRequest);
  }, [pageSize, searchRequest.pageNumber, searchRequest.counter]);

  return (
    <Layout>
      <Input.Group style={{ paddingBottom: "15px" }}>
        <Row gutter={8}>
          <Col span={3}>
            <Input
              style={{ width: "100%" }}
              value={searchRequest.id}
              placeholder="Id"
              onChange={(event) => handleIdBox(event)}
            />
          </Col>
          <Col>
            <Select
              value={searchRequest.isActive}
              onChange={(event) => handleSelectBox(event)}
            >
              <Select.Option value="all">-- All status --</Select.Option>
              <Select.Option value="true">Active</Select.Option>
              <Select.Option value="false">Inactive</Select.Option>
            </Select>
          </Col>
          <Col>
            <Button onClick={() => handleClearFilter()}>Clear filter</Button>
          </Col>
          <Col>
            <Button
              onClick={() => handleSearchButton()}
              icon={<SearchOutlined />}
            >
              Search
            </Button>
          </Col>
          <Col>
            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setVisible(true)}
            >
              Create
            </Button>
          </Col>
        </Row>
      </Input.Group>
      <Table
        rowKey="id"
        pagination={false}
        columns={columns}
        bordered
        dataSource={data}
      />
      <Pagination
        total={totalItem}
        showSizeChanger
        onShowSizeChange={(current, pageSize) => {
          setPageSize(pageSize);
        }}
        onChange={(page, pageSize) => {
          setSearchRequest(
            produce((draft) => {
              draft.pageNumber = page;
            })
          );
        }}
        pageSizeOptions={["5", "10", "15", "20"]}
        showTotal={(total, range) => `Total ${total} items`}
        pageSize={pageSize}
        current={searchRequest.pageNumber}
        defaultCurrent={searchRequest.pageNumber}
        style={{ paddingTop: "15px" }}
      ></Pagination>
      <CategoryForm
        visible={visible}
        onAfterCreateSuccess={() => handleAfterCreateSuccess()}
        onCancel={() => resetEditStatus()}
        data={detail}
        isEdit={isEdit}
      ></CategoryForm>
    </Layout>
  );
}
