import "./index.scss";
import {
  Button,
  Col,
  Input,
  Pagination,
  Row,
  Space,
  Table,
} from "antd";
import {
  PlusOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Action, Context } from "../../context/app-context";
import { Notification } from "../../common/notification";
import { useEffect, useState, useContext } from "react";
import IGetListRequest from "../../business/interfaces/setting/get-list-request";
import ISetting from "../../business/interfaces/setting/model";
import Layout from "../../templates/layout";
import MESSAGE from "../../common/message";
import Popup from "../../common/popup";
import produce from "immer";
import SettingBusiness from "../../business/setting-business";
import SettingForm from "../../organisms/setting-form";

interface IAppProps {}

interface ISearchRequest {
  counter: number;
  id?: number;
  key: string;
  pageNumber: number;
}

export default function NewsManagement(props: IAppProps) {
  const getColumns = () => {
    return [
      {
        title: "Key",
        dataIndex: "key",
        key: "key",
        ellipsis: {
          showTitle: false,
        },
      },
      {
        title: "Value",
        dataIndex: "value",
        key: "value",
        ellipsis: {
          showTitle: false,
        },
      },
      {
        title: "Action",
        dataIndex: "action",
        key: "action",
        align: "center",
        render: (text: string, record: any) => (
          <Space size="middle" className="my-space">
            <div className="ant-btn-group">
              <Button
                type="link"
                size={"small"}
                onClick={() => handleEditData(record.id)}
              >
                Edit
              </Button>
              {/* <Button
                type="link"
                danger
                size={"small"}
                onClick={() => handleDelete(record.id, record.key)}
              >
                Delete
              </Button> */}
            </div>
          </Space>
        ),
      },
    ];
  };
  const _bizSetting = new SettingBusiness();
  const columns: any[] = getColumns();
  const [data, setData] = useState([]);
  const [detail, setDetail] = useState<ISetting | undefined>(undefined);
  const [isEdit, setEdit] = useState(false);
  const [totalItem, setTotalItem] = useState(0);
  const [pageSize, setPageSize] = useState(10);
  const [searchRequest, setSearchRequest] = useState<ISearchRequest>({
    counter: 0,
    id: undefined,
    key: "",
    pageNumber: 1,
  });
  const [visible, setVisible] = useState(false);
  const state = useContext(Context);

  const getList = async (searchRequest: ISearchRequest) => {
    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: true,
    });

    try {
      const request: IGetListRequest = {
        requestCount: pageSize,
        page: searchRequest.pageNumber,
        key: searchRequest.key,
      }

      const result = await _bizSetting.GetList(request);
      if (result && result.data && result.data.records) {
        setData(result.data.records);
        setTotalItem(result.data.totalItem);
      } else {
        setData([]);
        setTotalItem(0);
      }
    } catch {
    } finally {
      state?.dispatch({
        type: Action.UPDATE_PRELOADER_STATUS,
        payload: false,
      });
    }
  };

  const handleClearFilter = async () => {
    setSearchRequest(
      produce((draft) => {
        draft.pageNumber = 1;
        draft.key = "";
        draft.id = "";
        draft.counter++;
      })
    );
  };

  const handleIdBox = (event: React.FormEvent<HTMLInputElement>) => {
    setSearchRequest(
      produce((draft) => {
        draft.id = event.currentTarget.value;
      })
    );
  };
  
  const handleKeyBox = (event: React.FormEvent<HTMLInputElement>) => {
    setSearchRequest(
      produce((draft) => {
        draft.key = event.currentTarget.value;
      })
    );
  };

  const handleSearchButton = async () => {
    await getList(searchRequest);
  };

  const handleEditData = async (id: number) => {
    if (!id || id < 1) {
      Notification.error({
        title: "Lỗi",
        content: MESSAGE.ERROR,
      });

      return;
    }

    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: true,
    });

    setEdit(true);

    const detail = await _bizSetting.Get(id);

    if (!detail) {
      Notification.error({
        title: "Lỗi",
        content: MESSAGE.ERROR,
      });
    } else {
      setDetail(detail);
    }

    setVisible(true);

    state?.dispatch({
      type: Action.UPDATE_PRELOADER_STATUS,
      payload: false,
    });
  };

  const handleDelete = async (id: Number, name: string) => {
    Popup.confirm("Thông báo", `${MESSAGE.CONFIRM} ("${name}")`, async () => {
      const bIsSuccess = await _bizSetting.Delete(id);

      if (bIsSuccess === true) {
        Notification.success({
          content: "Xóa thành công",
        });

        setSearchRequest(
          produce((draft) => {
            draft.counter = draft.counter + 1;
          })
        );
      } else {
        Notification.error({
          content: "Xóa thất bại",
        });
      }
    });
  };

  const resetEditStatus = () => {
    setVisible(false);
    setEdit(false);
  };

  const handleAfterCreateSuccess = async () => {
    resetEditStatus();
    setSearchRequest(
      produce((draft) => {
        draft.counter = draft.counter + 1;
      })
    );
  };

  useEffect(() => {
    getList(searchRequest);
  }, [pageSize, searchRequest.pageNumber, searchRequest.counter]);

  return (
    <Layout>
      <Input.Group style={{ paddingBottom: "15px" }}>
        <Row gutter={8}>
          <Col span={3}>
            <Input
              style={{ width: "100%" }}
              value={searchRequest.key}
              placeholder="Key"
              onChange={(event) => handleKeyBox(event)}
            />
          </Col>
          <Col>
            <Button onClick={() => handleClearFilter()}>Clear filter</Button>
          </Col>
          <Col>
            <Button
              onClick={() => handleSearchButton()}
              icon={<SearchOutlined />}
            >
              Search
            </Button>
          </Col>
          {/* <Col>
            <Button
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setVisible(true)}
            >
              Create
            </Button>
          </Col> */}
        </Row>
      </Input.Group>
      <Table
        rowKey="id"
        pagination={false}
        columns={columns}
        bordered
        dataSource={data}
      />
      <Pagination
        total={totalItem}
        showSizeChanger
        onShowSizeChange={(current, pageSize) => {
          setPageSize(pageSize);
        }}
        onChange={(page, pageSize) => {
          setSearchRequest(
            produce((draft) => {
              draft.pageNumber = page;
            })
          );
        }}
        pageSizeOptions={["5", "10", "15", "20"]}
        showTotal={(total, range) => `Total ${total} items`}
        pageSize={pageSize}
        current={searchRequest.pageNumber}
        defaultCurrent={searchRequest.pageNumber}
        style={{ paddingTop: "15px" }}
      ></Pagination>
      <SettingForm
        visible={visible}
        onAfterCreateSuccess={() => handleAfterCreateSuccess()}
        onCancel={() => resetEditStatus()}
        data={detail}
        isEdit={isEdit}
      ></SettingForm>
    </Layout>
  );
}
