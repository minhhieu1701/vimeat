import { IFormData } from "../common/definition";
import { vimeat as VIMEAT } from "../common/util";
import $ from "lodash";
import ICreateRequest from "./interfaces/news/create-request";
import IGetListRequest from "./interfaces/news/get-list-request";
import moment from "moment";

class NewsBusiness {
  async GetList(request: IGetListRequest) {
    const resp = await VIMEAT.post(`/news/search`, request);

    if ($.isEmpty(resp) === true) {
      return null;
    }

    return resp;
  }

  async Create(requestData: IFormData) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.title) === true ||
      $.isEmpty(requestData.publishFrom) === true ||
      requestData.publishFrom.isValid() === false
    ) {
      return false;
    }

    const createRequest: ICreateRequest = {
      description: requestData.description,
      isActive: requestData.isActive,
      photo: requestData.photo,
      title: requestData.title,
      publishFrom: requestData.publishFrom.format("YYYY-MM-DD HH:mm"),
      urlRewrite: requestData.urlRewrite,
    };

    if (
      $.isEmpty(requestData.publishTo) === false &&
      requestData.publishTo?.isValid() === true
    ) {
      createRequest.publishTo = requestData.publishTo.format(
        "YYYY-MM-DD HH:mm"
      );
    }

    const resp = await VIMEAT.put(`/news/insert`, createRequest);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Update(requestData: IFormData) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.title) === true ||
      $.isEmpty(requestData.publishFrom) === true ||
      requestData.publishFrom.isValid() === false
    ) {
      return false;
    }

    const updateRequest: ICreateRequest = {
      description: requestData.description,
      isActive: requestData.isActive,
      photo: requestData.photo,
      title: requestData.title,
      publishFrom: requestData.publishFrom.format("YYYY-MM-DD HH:mm"),
      urlRewrite: requestData.urlRewrite,
    };

    if (
      $.isEmpty(requestData.publishTo) === false &&
      requestData.publishTo?.isValid() === true
    ) {
      updateRequest.publishTo = requestData.publishTo.format(
        "YYYY-MM-DD HH:mm"
      );
    }

    const resp = await VIMEAT.put(
      `/news/${requestData.uid}/update`,
      updateRequest
    );

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Delete(uid: string) {
    if ($.isEmpty(uid) === true) {
      return false;
    }

    const resp = await VIMEAT.delete(`/news/${uid}/delete`);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Get(uid: string) {
    if ($.isEmpty(uid) === true) {
      return null;
    }

    const resp = await VIMEAT.post(`/news/${uid}`, null);

    if (
      $.isEmpty(resp) === true ||
      resp.code !== 0 ||
      $.isEmpty(resp.data) === true
    ) {
      return null;
    }

    if (resp.data.publishFrom) {
      resp.data.publishFrom = moment(resp.data.publishFrom);
    }

    if (resp.data.publishTo) {
      resp.data.publishTo = moment(resp.data.publishTo);
    }

    return resp.data;
  }
}

export default NewsBusiness;
