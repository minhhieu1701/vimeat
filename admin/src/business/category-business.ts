import { vimeat as VIMEAT } from "../common/util";
import $ from "lodash";
import ICreateRequest from "./interfaces/category/create-request";
import IGetListRequest from "./interfaces/news/get-list-request";
import moment from "moment";
import IEnumItem from "./interfaces/shared/enum-item";

class CategoryBusiness {
  async GetList(request: IGetListRequest) {
    const resp = await VIMEAT.post(`/category/search`, request);

    if ($.isEmpty(resp) === true) {
      return null;
    }

    return resp;
  }

  async GetEnumList(): Promise<Array<IEnumItem<number>> | null> {
    const resp = await VIMEAT.get(`/category/enum/list`);

    if ($.isEmpty(resp) === true) {
      return null;
    }

    const result = new Array<IEnumItem<number>>();

    if (!resp || resp.code !== 0 || !resp.data) {
      return result;
    }

    resp.data.forEach((item: any) => {
      result.push({
        text: item.text,
        value: item.value,
      });
    });

    return result;
  }

  async Create(requestData: ICreateRequest) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.name) === true
    ) {
      return false;
    }

    const resp = await VIMEAT.put(`/category/insert`, requestData);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Update(id: number, requestData: ICreateRequest) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.name) === true
    ) {
      return false;
    }

    const resp = await VIMEAT.put(`/category/${id}/update`, requestData);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Delete(id: Number) {
    if (id < 1) {
      return false;
    }

    const resp = await VIMEAT.delete(`/category/${id}/delete`);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Get(id: number) {
    if (!id || id < 1) {
      return null;
    }

    const resp = await VIMEAT.get(`/category/${id}`);

    if (
      $.isEmpty(resp) === true ||
      resp.code !== 0 ||
      $.isEmpty(resp.data) === true
    ) {
      return null;
    }

    return resp.data;
  }
}

export default CategoryBusiness;
