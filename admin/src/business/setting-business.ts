import { vimeat as VIMEAT } from "../common/util";
import $ from "lodash";
import ICreateRequest from "./interfaces/setting/create-request";
import IGetListRequest from "./interfaces/setting/get-list-request";

class SettingBusiness {
  async GetList(request: IGetListRequest) {
    const resp = await VIMEAT.post(`/setting/search`, request);

    if ($.isEmpty(resp) === true) {
      return null;
    }

    return resp;
  }

  async Create(requestData: ICreateRequest) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.key) === true ||
      $.isEmpty(requestData.value) === true
    ) {
      return false;
    }

    const resp = await VIMEAT.put(`/setting/insert`, requestData);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Update(id: number, value: string) {
    if (!id || $.isEmpty(value) === true) {
      return false;
    }

    const resp = await VIMEAT.put(`/setting/${id}/update`, { value });

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Delete(id: Number) {
    if (!id) {
      return false;
    }

    const resp = await VIMEAT.delete(`/setting/${id}/delete`);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Get(id: number) {
    if (!id) {
      return null;
    }

    const resp = await VIMEAT.get(`/setting/${id}`);

    if (
      $.isEmpty(resp) === true ||
      resp.code !== 0 ||
      $.isEmpty(resp.data) === true
    ) {
      return null;
    }

    return resp.data;
  }
}

export default SettingBusiness;
