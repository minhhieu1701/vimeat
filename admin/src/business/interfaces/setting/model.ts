export default interface ISetting {
  id: number;
  key: string;
  value: string;
}
