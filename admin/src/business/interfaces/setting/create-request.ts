export default interface ICreateRequest {
  key: string;
  value: string;
}
