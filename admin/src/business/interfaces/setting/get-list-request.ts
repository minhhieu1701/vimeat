export default interface IGetListRequest {
    key?: string;
    page: number;
    requestCount: number;
  }
  