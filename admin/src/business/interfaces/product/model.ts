import { IPhotoItem } from "../../../common/definition";
import IBasic from "./basic";
import { Moment } from "moment";

export interface IProduct extends IBasic {
  photos: Array<IPhotoItem[]>;
  publishFrom?: Moment;
  publishTo?: Moment;
}
