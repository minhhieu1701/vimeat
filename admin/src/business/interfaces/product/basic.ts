export default interface IBasic {
  createdAt: string;
  description: string;
  havePromotion: Boolean;
  id?: number;
  isActive: boolean;
  name: string;
  salePrice?: number;
  shortDescription: string;
  unit: number;
  unitPrice: number;
  urlRewrite: string;
  weight: number;
}
