import IBasic from "./basic";

export default interface IUpdateRequest extends IBasic {
  categories: [];
  photo: string[];
  publishFrom: string;
  publishTo: string
}
