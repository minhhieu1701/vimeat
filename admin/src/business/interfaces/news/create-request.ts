export default interface ICreateRequest {
  title: string;
  description: string;
  publishFrom?: string;
  publishTo?: string | null;
  isActive: boolean;
  photo: string;
  urlRewrite: string;
}
