export default interface IGetListRequest {
    isActive?: boolean;
    keyword?: string;
    page: number;
    requestCount: number;
    uid?: string;
  }
  