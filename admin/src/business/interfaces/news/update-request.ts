import ICreateRequest from "./create-request";

export default interface IUpdateRequest extends ICreateRequest {
  uid: string;
}
