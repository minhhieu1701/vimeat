import { IPhotoItem } from "../../../common/definition";

export interface ICategory {
  id: number;
  name: string;
  isActive: boolean;
  photo: Array<IPhotoItem>;
  background: string;
  borderColor: string;
}
