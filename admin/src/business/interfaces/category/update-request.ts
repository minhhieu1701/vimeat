import ICreateRequest from "./create-request";

export default interface IUpdateRequest extends ICreateRequest {
  id: Number;
}
