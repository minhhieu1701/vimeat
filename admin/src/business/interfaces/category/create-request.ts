export default interface ICreateRequest {
  isActive: boolean;
  name: string;
  photo: string;
  background: string;
  borderColor: string;
}
