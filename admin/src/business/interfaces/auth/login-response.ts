export default interface ILoginResponse {
  fullname: string;
  token: string;
  uid: string;
}
