export default interface IEnumItem<T> {
  text: string;
  value: T;
}
