import { vimeat as VIMEAT } from "../common/util";
import $ from "lodash";

class AuthBusiness {
  async Login(username: string, password: string) {
    const resp = await VIMEAT.post("/auth", {
      username: username,
      password: password,
    });

    if ($.isEmpty(resp) === true) {
      return null;
    }

    return resp; 
  }
}

export default AuthBusiness;
