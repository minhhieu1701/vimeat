import { vimeat as VIMEAT, defaultOption, cloneObject } from "../common/util";

class UploadBusiness {
  async Upload(
    folderName: string,
    file: any,
    onSuccess: Function,
    onError: Function
  ) {
    const option = cloneObject(defaultOption);

    option.headers["Content-Type"] = "multipart/form-data";

    const formData = new FormData();
    formData.append("file", file);
    formData.append("folderName", folderName);

    const resp = await VIMEAT.post(`/upload/image`, formData, option);

    if (
      resp &&
      resp.code === 0 &&
      resp.data &&
      resp.data.length > 0 &&
      resp.data[0].isSuccess === true
    ) {
      onSuccess(resp.data[0]);
    } else {
      onError(resp.message);
    }
  }

  async UploadForTinyMce(
    blobInfo: any,
    success: any,
    failure: any,
    folderName: string
  ) {
    let valid_extensions = ["png", "jpg"];
    let ext, extensions: any;

    extensions = {
      "image/jpeg": "jpg",
      "image/jpg": "jpg",
      "image/gif": "gif",
      "image/png": "png",
    };

    ext = extensions[blobInfo.blob().type.toLowerCase()] || "dat";
    //add your extension test here.
    if (valid_extensions.indexOf(ext) === -1) {
      failure("Invalid extension");
      return;
    }

    const formData = new FormData();
    formData.append("file", blobInfo.blob(), blobInfo.filename());
    formData.append("folderName", folderName);

    const option = cloneObject(defaultOption);

    option.headers["Content-Type"] = "multipart/form-data";

    const resp = await VIMEAT.post(`/upload/image`, formData, option);

    if (
      resp &&
      resp.code === 0 &&
      resp.data &&
      resp.data.length > 0 &&
      resp.data[0].isSuccess === true
    ) {
      success(resp.data[0].url);
    } else {
      failure("Upload failed!");
      alert(resp.message);
    }
  }
}

export default UploadBusiness;
