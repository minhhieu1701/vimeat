import { vimeat as VIMEAT } from "../common/util";
import $ from "lodash";
import IGetListRequest from "./interfaces/product/get-list-request";
import IUpsertRequest from "./interfaces/product/upsert-request";
import moment from "moment";

class NewsBusiness {
  async GetList(request: IGetListRequest) {
    const resp = await VIMEAT.post(`/product/search`, request);

    if ($.isEmpty(resp) === true) {
      return null;
    }

    return resp;
  }

  async Create(requestData: IUpsertRequest) {
    if (
      $.isEmpty(requestData) === true ||
      $.isEmpty(requestData.name) === true
    ) {
      return false;
    }

    const resp = await VIMEAT.put(`/product/insert`, requestData);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Update(id: number, requestData: IUpsertRequest) {
    if (
      $.isEmpty(requestData) === true ||
      !id ||
      id < 1 ||
      $.isEmpty(requestData.name) === true
    ) {
      return false;
    }

    const resp = await VIMEAT.put(
      `/product/${id}/update`,
      requestData
    );

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Delete(id: Number) {
    if (!id || id < 1) {
      return false;
    }

    const resp = await VIMEAT.delete(`/product/${id}/delete`);

    if (resp && resp.code === 0) {
      return true;
    }

    return false;
  }

  async Get(id: Number) {
    if (!id || id < 1) {
      return null;
    }

    const resp = await VIMEAT.post(`/product/${id}`, null);

    if (
      $.isEmpty(resp) === true ||
      resp.code !== 0 ||
      $.isEmpty(resp.data) === true
    ) {
      return null;
    }

    if (resp.data.publishFrom) {
      resp.data.publishFrom = moment(+resp.data.publishFrom);
    }

    if (resp.data.publishTo) {
      resp.data.publishTo = moment(+resp.data.publishTo);
    }

    return resp.data;
  }
}

export default NewsBusiness;
